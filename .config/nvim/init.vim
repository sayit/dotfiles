" Neovim configuration
" By sayit358
" :cope

" Set all the things!

set number
set lazyredraw
set confirm
set splitbelow splitright
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set foldlevel=99
set clipboard+=unnamedplus
set noswapfile
set noshowmode
set termguicolors
set shiftwidth=2 softtabstop=2 tabstop=2
set expandtab
set smartindent
set hidden
set timeoutlen=500

call plug#begin('~/.config/nvim/plugged')

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'tpope/vim-surround'
Plug 'ggandor/lightspeed.nvim'
Plug 'justinmk/vim-dirvish'
Plug 'ThePrimeagen/harpoon'
Plug 'ibhagwan/fzf-lua'
Plug 'nvim-lua/plenary.nvim'
Plug 'gbprod/cutlass.nvim'
Plug 'numToStr/Comment.nvim'
Plug 'junegunn/vim-easy-align', { 'on':  'EasyAlign' }
Plug 'sainnhe/everforest'
Plug 'vim-test/vim-test'
Plug 'rcarriga/vim-ultest', { 'do': ':UpdateRemotePlugins' }
Plug 'roginfarrer/vim-dirvish-dovish', {'branch': 'main'}
Plug 'nvim-lualine/lualine.nvim'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'windwp/nvim-autopairs'

" Lsp stuff
Plug 'neovim/nvim-lspconfig'
Plug 'rafamadriz/friendly-snippets'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/cmp-path'
Plug 'mfussenegger/nvim-dap'
Plug 'rcarriga/nvim-dap-ui'
Plug 'theHamsta/nvim-dap-virtual-text'
Plug 'SmiteshP/nvim-gps'
Plug 'ray-x/lsp_signature.nvim'
Plug 'stevearc/dressing.nvim'

call plug#end()

let g:everforest_transparent_background = 1
let g:everforest_background = 'soft'
colorscheme everforest

autocmd BufWritePre * %s/\s\+$//e

" Dot command
vnoremap <silent> . :norm .<CR>

" Navigate wrapped lines
nnoremap <expr> k (v:count == 0 ? 'gk' : 'k')
nnoremap <expr> j (v:count == 0 ? 'gj' : 'j')

" Window navigation
noremap <C-Left> <C-w>h
noremap <C-Down> <C-w>j
noremap <C-Up> <C-w>k
noremap <C-Right> <C-w>l

au TextYankPost * silent! lua vim.highlight.on_yank{on_visual=false, higroup="IncSearch", timeout=500}

nnoremap <SPACE> <Nop>
let mapleader = "\<Space>"

au BufWritePre,FileWritePre * silent! call mkdir(expand('<afile>:p:h'), 'p')

nnoremap <silent> <Space>q :q<CR>
nnoremap <silent> <Space>Q :bdelete<CR>

lua vim.api.nvim_set_keymap('i', '<C-H>', '<C-W>', {noremap = true})

" " add mapping for auto closing, by u/Mitschix
" imap "<tab> ""<Left>
" imap '<tab> ''<Left>
" imap (<tab> ()<Left>
" imap [<tab> []<Left>
" imap {<tab> {}<Left>
" imap {<CR> {<CR>}<ESC>O
" imap {;<CR> {<CR>};<ESC>O
