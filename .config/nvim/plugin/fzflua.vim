nmap <Leader>ff :FzfLua files<CR>
nnoremap <Leader>fg :FzfLua live_grep<CR>
nnoremap <Leader>bb :FzfLua buffers<CR>

lua << EOF

function neovimpd()
  require('fzf-lua').files({
  cmd = 'mpc -f "%position%. %artist% - %title% (%album%)" playlist',
  file_icons = false,
  preview_opts = 'hidden',
    prompt    = 'mpd> ',
    actions = {
      ["default"]  = function(selected) os.execute('~/.local/bin/neovimpd.sh "' .. tostring(selected[1]) .. '"') end
    }
  })
end
EOF
nnoremap <silent> mu :lua neovimpd()<CR>
