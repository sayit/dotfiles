
local cutChords = true

function keyHandler(dialog, key)
    return key
end

function incrementScaleTonicNote()

  if preferences.scaleTonicNote.value == #notes then
    return
  end

  preferences.scaleTonicNote.value = preferences.scaleTonicNote.value + 1

  if (viewBuilder == nil) then
    preferences:resetSelectedChordTypes()
    preferences:resetSelectedInversionStates()
    updateScaleData()
    showScaleStatus()
    updateScaleDegreeHeaders()
  end
end

function decrementScaleTonicNote()

  if preferences.scaleTonicNote.value == 1 then
    return
  end

  preferences.scaleTonicNote.value = preferences.scaleTonicNote.value - 1

  if (viewBuilder == nil) then
    preferences:resetSelectedChordTypes()
    preferences:resetSelectedInversionStates()
    updateScaleData()
    showScaleStatus()
    updateScaleDegreeHeaders()
  end
end

function incrementScaleType()

  if preferences.scaleType.value == #notes then
    return
  end

  preferences.scaleType.value = preferences.scaleType.value + 1
  
  if (viewBuilder == nil) then
    preferences:resetSelectedChordTypes()
    preferences:resetSelectedInversionStates()
    updateScaleData()
    showScaleStatus()
    updateScaleDegreeHeaders()
  end
end

function decrementScaleType()

  if preferences.scaleType.value == 1 then
    return
  end

  preferences.scaleType.value = preferences.scaleType.value - 1

  if (viewBuilder == nil) then
    preferences:resetSelectedChordTypes()
    preferences:resetSelectedInversionStates()
    updateScaleData()
    showScaleStatus()
    updateScaleDegreeHeaders()
  end
end

function incrementChordType()

  local selectedScaleDegree = preferences.selectedScaleDegree.value

  if preferences.selectedChordTypes[selectedScaleDegree].value < #scaleChords[selectedScaleDegree] then
    preferences.selectedChordTypes[selectedScaleDegree].value = preferences.selectedChordTypes[selectedScaleDegree].value + 1
  end

  insertChord(selectedScaleDegree)
end

function decrementChordType()

  local selectedScaleDegree = preferences.selectedScaleDegree.value

  if preferences.selectedChordTypes[selectedScaleDegree].value > 1 then
    preferences.selectedChordTypes[selectedScaleDegree].value = preferences.selectedChordTypes[selectedScaleDegree].value - 1
  end
  
  insertChord(selectedScaleDegree)
end

function incrementChordInversion()

  if preferences.chordInversion.value ~= preferences.chordInversionMax.value then
    preferences.chordInversion.value = preferences.chordInversion.value + 1
    setInversion(preferences.chordInversion.value)
  end
end

function decrementChordInversion()
    
  if preferences.chordInversion.value ~= preferences.chordInversionMin.value then
    preferences.chordInversion.value = preferences.chordInversion.value - 1
    setInversion(preferences.chordInversion.value)
  end
end

renoise.tool():add_keybinding {
  name = "Global:ChordLord:increment scale tonic note",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    incrementScaleTonicNote()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:decrement scale tonic note",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    decrementScaleTonicNote()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:increment scale type",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    incrementScaleType()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:decrement scale type",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()   
    end
    
    decrementScaleType()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:increment chord type",
  invoke = function()

    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    incrementChordType()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:decrement chord type",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    decrementChordType()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:increment chord inversion",
  invoke = function()
  
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    incrementChordInversion()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:decrement chord inversion",
  invoke = function()

    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    decrementChordInversion()
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 1",
  invoke = function(repeated)
  
    if repeated then
      return
    end
   
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 1
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 2",
  invoke = function(repeated)
  
    if repeated then
      return
    end
        
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 2
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 3",
  invoke = function(repeated)
  
    if repeated then
      return
    end
       
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 3
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 4",
  invoke = function(repeated)
  
    if repeated then
      return
    end
        
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 4
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 5",
  invoke = function(repeated)
  
    if repeated then
      return
    end
        
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 5
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 6",
  invoke = function(repeated)
  
    if repeated then
      return
    end
          
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end

    local scaleDegree = 6
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
renoise.tool():add_keybinding {
  name = "Global:ChordLord:insert scale chord 7",
  invoke = function(repeated)
  
    if repeated then
      return
    end
    
    if (preferences == nil) then
      preferences = ToolPreferences()
      preferences:loadValues()
      updateScaleData()
    end
    
    local scaleDegree = 7
    insertChord(scaleDegree)
    preferences:saveValues()
  end
}
