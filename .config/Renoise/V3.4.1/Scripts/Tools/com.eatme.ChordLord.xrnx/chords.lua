chords = {
  {
    complexity = 0,
    name = 'Major',
    code = 'maj',
    display = '',
    pattern = '10001001'
  },
  {
    complexity = 0,
    name = 'minor',
    code = 'min',
    display = 'm',
    pattern = '10010001'
  },
  {
    complexity = 0,
    name = 'Power Chord',
    code = 'power',
    display = '5',
    pattern = '10000001'
  },
  {
    complexity = 0,
    name = 'Augmented',
    code = 'aug',
    display = 'aug',
    pattern = '100010001'
  },
  {
    complexity = 0,
    name = 'Diminished',
    code = 'dim',
    display = 'dim',
    pattern = '1001001'
  },
  {
    complexity = 1,
    name = 'suspended second',
    code = 'sus2',
    display = 'sus2',
    pattern = '10100001'
  },
  {
    complexity = 1,
    name = 'suspended fourth',
    code = 'sus4',
    display = 'sus4',
    pattern = '10000101'
  },
  {
    complexity = 2,
    name = 'Major add second',
    code = 'majadd2',
    display = 'Majadd2',
    pattern = '10101001'
  },
  {
    complexity = 2,
    name = 'Major add fourth',
    code = 'majadd4',
    display = 'Majadd4',
    pattern = '10001101'
  },
  {
    complexity = 2,
    name = 'minor add second',
    code = 'minadd2',
    display = 'minadd2',
    pattern = '10110001'
  },
  {
    complexity = 2,
    name = 'minor add fourth',
    code = 'minadd4',
    display = 'minadd4',
    pattern = '10010101'
  },
  {
    complexity = 2,
    name = 'Major add second diminished fifth',
    code = 'majadd2b5',
    display = 'Majadd2b5',
    pattern = '1010100110'
  },
  {
    complexity = 2,
    name = 'minor add second diminished fifth',
    code = 'minadd2b5',
    display = 'minadd2b5',
    pattern = '10110010'
  },
  { 
    complexity = 0,
    name = 'sixth',
    code = 'maj6',
    display = '6',
    pattern = '1000100101'
  },
  {
    complexity = 0,
    name = 'minor sixth',
    code = 'min6',
    display = 'm6',
    pattern = '1001000101'
  },
  {
    complexity = 2,
    name = 'Major sixth ninth',
    code = 'maj69',
    display = 'Maj69',
    pattern = '100010010100001'
  },
  {
    complexity = 2,
    name = 'minor sixth ninth',
    code = 'min69',
    display = 'min69',
    pattern = '100100010100001'
  },
  {
    complexity = 0,
    name = 'seventh',
    code = '7',
    display = '7',
    pattern = '10001001001'
  },
  {
    complexity = 0,
    name = 'Major seventh',
    code = 'maj7',
    display = 'Maj7',
    pattern = '100010010001'
  },
  {
    complexity = 0,
    name = 'minor seventh',
    code = 'min7',
    display = 'm7',
    pattern = '10010001001'
  },
  {
    complexity = 1,
    name = 'minor/Major seventh',
    code = 'min/Maj7',
    display = 'min/Maj7',
    pattern = '100100010001'
  },
  {
    complexity = 0,
    name = 'diminished seventh',
    code = 'dim7',
    display = 'dim7',
    pattern = '1001001001'
  },
  {
    complexity = 1,
    name = 'half-diminished',
    code = 'min7b5',
    display = 'min7b5',
    pattern = '10010010001'
  },
  {
    complexity = 1,
    name = '(no root) seventh added diminished second',
    code = 'x7addb2',
    display = 'x7addb2',
    pattern = '01001001001'
  },
  {
    complexity = 2,
    name = 'seventh suspended fourth',
    code = '7sus4',
    display = '7sus4',
    pattern = '10000101001'
  },
  {
    complexity = 2,
    name = 'Major seventh suspended fourth',
    code = 'maj7sus4',
    display = 'Maj7sus4',
    pattern = '100001010001'
  },
  {
    complexity = 2,
    name = 'seventh diminished fifth',
    code = '7b5',
    display = '7b5',
    pattern = '10001010001'     
  },
  {
    complexity = 2,
    name = 'seventh augmented fifth',
    code = '7#5',
    display = '7#5',
    pattern = '10001000101'
  },
  {
    complexity = 2,
    name = 'major seventh diminished fifth',
    code = 'maj7b5',
    display = 'Maj7b5',
    pattern = '100010100001'
  },
  {
    complexity = 2,
    name = 'major seventh augmented fifth',
    code = 'maj7#5',
    display = 'Maj7#5',
    pattern = '100010001001'
  },
  {
    complexity = 1,
    name = 'seventh Power Chord',
    code = 'power7',
    display = '57',
    pattern = '100000010010'
  },
  {
    complexity = 1,
    name = 'Major seventh Power Chord',
    code = 'powerMaj7',
    display = '5Maj7',
    pattern = '100000010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended second diminished fifth augmented ninth',
    code = '7sus2b5#9',
    display = '7sus2b5#9',
    pattern = '1010001000100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended second diminished fifth augmented ninth',
    code = 'm/maj7sus2b5#9',
    display = 'm/Maj7sus2b5#9',
    pattern = '1010001000010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended fourth diminished fifth augmented ninth',
    code = '7sus4b5#9',
    display = '7sus4b5#9',
    pattern = '1000011000100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended fourth diminished fifth augmented ninth',
    code = 'm/maj7sus4b5#9',
    display = 'm/Maj7sus4b5#9',
    pattern = '1000011000010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended second diminished fifth augmented ninth diminished thirteenth',
    code = '7sus2b5#9b13',
    display = '7sus2b5#9b13',
    pattern = '1010001000100001000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended second diminished fifth augmented ninth diminished thirteenth',
    code = 'm/maj7sus2b5#9b13',
    display = 'm/Maj7sus2b5#9b13',
    pattern = '1010001000010001000010'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended fourth diminished fifth augmented ninth diminished thirteenth',
    code = '7sus4b5#9b13',
    display = '7sus4b5#9b13',
    pattern = '1000011000100001000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended fourth diminished fifth augmented ninth diminished thirteenth',
    code = 'm/maj7sus4b5#9b13',
    display = 'm/Maj7sus4b5#9b13',
    pattern = '1000011000010001000010'
  },
  {
    complexity = 2,
    name = 'seventh suspended fourth augmented fifth',
    code = '7sus4#5',
    display = '7sus4#5',
    pattern = '10000100101'
  },
  {
    complexity = 2,
    name = 'Major seventh suspended fourth augmented fifth',
    code = 'maj7sus4#5',
    display = 'Maj7sus4#5',
    pattern = '100001001001'
  },
  {
    complexity = 2,
    name = 'seventh eight (octave) Power Chord',
    code = 'power78',
    display = '578',
    pattern = '1000000100101'
  },
  {
    complexity = 2,
    name = 'Major seventh eight (octave) Power Chord',
    code = 'powerMaj78',
    display = '5Maj78',
    pattern = '1000000100011'
  },
  {
    complexity = 3,
    name = 'seventh suspended second augmented fifth diminished ninth',
    code = '7sus2#5b9',
    display = '7sus2#5b9',
    pattern = '101000001010010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second augmented fifth diminished ninth',
    code = 'maj7sus2#5b9',
    display = 'Maj7sus2#5b9',
    pattern = '101000001001010'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth augmented fifth diminished ninth',
    code = '7sus4#5b9',
    display = '7sus4#5b9',
    pattern = '100001001010010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth augmented fifth diminished ninth',
    code = 'maj7sus4#5b9',
    display = 'Maj7sus4#5b9',
    pattern = '100001001001010'
  },
  {
    complexity = 3,
    name = 'seventh suspended second augmented fifth diminished ninth augmented eleventh',
    code = '7sus2#5b9#11',
    display = '7sus2#5b9#11',
    pattern = '1010000010100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second augmented fifth diminished ninth augmented eleventh',
    code = 'maj7sus2#5b9#11',
    display = 'Maj7sus2#5b9#11',
    pattern = '1010000010010100001'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth augmented fifth diminished ninth augmented eleventh',
    code = '7sus4#5b9#11',
    display = '7sus4#5b9#11',
    pattern = '1000010010100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth augmented fifth diminished ninth augmented eleventh',
    code = 'maj7sus4#5b9#11',
    display = 'Maj7sus4#5b9#11',
    pattern = '1000010010010100001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended second augmented fifth augmented ninth',
    code = '7sus2#5#9',
    display = '7sus2#5#9',
    pattern = '1010000010100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended second augmented fifth augmented ninth',
    code = 'm/maj7sus2#5#9',
    display = 'm/Maj7sus2#5#9',
    pattern = '1010000010010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended fourth augmented fifth augmented ninth',
    code = '7sus4#5#9',
    display = '7sus4#5#9',
    pattern = '1000010010100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended fourth augmented fifth augmented ninth',
    code = 'm/Maj7sus4#5#9',
    display = 'm/Maj7sus4#5#9',
    pattern = '1000010010010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended second augmented fifth augmented ninth augmented eleventh',
    code = '7sus2#5#9#11',
    display = '7sus2#5#9#11',
    pattern = '1010000010100001001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended second augmented fifth augmented ninth augmented eleventh',
    code = 'm/maj7sus2#5#9#11',
    display = 'm/Maj7sus2#5#9#11',
    pattern = '1010000010010001001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended fourth augmented fifth augmented ninth augmented eleventh',
    code = '7sus4#5#9#11',
    display = '7sus4#5#9#11',
    pattern = '1000010010100001001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended fourth augmented fifth augmented ninth augmented eleventh',
    code = 'm/maj7sus4#5#9#11',
    display = 'm/Maj7sus4#5#9#11',
    pattern = '1000010010010001001'
  },
  {
    complexity = 3,
    name = 'seventh suspended second diminished ninth',
    code = '7sus2b9',
    display = '7sus2b9',
    pattern = '101000010010010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second diminished ninth',
    code = 'maj7sus2b9',
    display = 'Maj7sus2b9',
    pattern = '101000010001010'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth diminished ninth',
    code = '7sus4b9',
    display = '7sus4b9',
    pattern = '100001010010010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth diminished ninth',
    code = 'maj7sus4b9',
    display = 'Maj7sus4b9',
    pattern = '100001010001010'
  },
  {
    complexity = 3,
    name = 'seventh suspended second diminished thirteenth',
    code = '7sus2b13',
    display = '7sus2b13',
    pattern = '1010000100100000000010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second diminished thirteenth',
    code = 'maj7sus2b13',
    display = 'Maj7sus2b13',
    pattern = '1010000100010000000010'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth diminished thirteenth',
    code = '7sus4b13',
    display = '7sus4b13',
    pattern = '1000010100100000000010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth diminished thirteenth',
    code = 'maj7sus4b13',
    display = 'Maj7sus4b13',
    pattern = '1000010100010000000010'
  },
  {
    complexity = 3,
    name = 'seventh suspended second diminished ninth diminished thirteenth',
    code = '7sus2b9b13',
    display = '7sus2b9b13',
    pattern = '1010000100100100000010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second diminished ninth diminished thirteenth',
    code = 'maj7sus2b9b13',
    display = 'Maj7sus2b9b13',
    pattern = '1010000100010100000010'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth diminished ninth diminished thirteenth',
    code = '7sus4b9b13',
    display = '7sus4b9b13',
    pattern = '1000010100100100000010'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth diminished ninth diminished thirteenth',
    code = 'maj7sus4b9b13',
    display = 'Maj7sus4b9b13',
    pattern = '1000010100010100000010'
  },
  {
    complexity = 3,
    name = 'seventh suspended second diminished ninth augmented eleventh',
    code = '7sus2b9#11',
    display = '7sus2b9#11',
    pattern = '1010000100100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second diminished ninth augmented eleventh',
    code = 'maj7sus2b9#11',
    display = 'Maj7sus2b9#11',
    pattern = '1010000100010100001'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth diminished ninth augmented eleventh',
    code = '7sus4b9#11',
    display = '7sus4b9#11',
    pattern = '1000010100100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth diminished ninth augmented eleventh',
    code = 'maj7sus4b9#11',
    display = 'Maj7sus4b9#11',
    pattern = '1000010100010100001'
  },
  {
    complexity = 3,
    name = 'seventh suspended second diminished ninth augmented eleventh diminished thirteenth',
    code = '7sus2b9#11b13',
    display = '7sus2b9#11b13',
    pattern = '101000010010010000101'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second diminished ninth augmented eleventh diminished thirteenth',
    code = 'maj7sus2b9#11b13',
    display = 'Maj7sus2b9#11b13',
    pattern = '101000010001010000101'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth diminished ninth augmented eleventh diminished thirteenth',
    code = '7sus4b9#11b13',
    display = '7sus4b9#11b13',
    pattern = '100001010010010000101'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth diminished ninth augmented eleventh diminished thirteenth',
    code = 'maj7sus4b9#11b13',
    display = 'Maj7sus4b9#11b13',
    pattern = '100001010001010000101'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended second augmented ninth',
    code = '7sus2#9',
    display = '7sus2#9',
    pattern = '1010000100100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended second augmented ninth',
    code = 'm/maj7sus2#9',
    display = 'm/Maj7sus2#9',
    pattern = '1010000100010001'
  },
  {
    complexity = 3,
    name = 'minor seventh suspended fourth augmented ninth',
    code = '7sus4b5#9',
    display = '7sus4#9',
    pattern = '1000010100100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh suspended fourth augmented ninth',
    code = 'm/maj7sus4#9',
    display = 'm/Maj7sus4#9',
    pattern = '1000010100010001'
  },
  {
    complexity = 3,
    name = 'seventh suspended second augmented eleventh',
    code = '7sus2#11',
    display = '7sus2#11',
    pattern = '1010000100100000001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second augmented eleventh',
    code = 'maj7sus2#11',
    display = 'Maj7sus2#11',
    pattern = '1010000100010000001'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth augmented eleventh',
    code = '7sus4#11',
    display = '7sus4#11',
    pattern = '1000010100100000001'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth augmented eleventh',
    code = 'maj7sus4#11',
    display = 'Maj7sus4#11',
    pattern = '1000010100010000001'
  },
  {
    complexity = 3,
    name = 'seventh suspended second augmented eleventh diminished thirteenth',
    code = '7sus2#11b13',
    display = '7sus2#11b13',
    pattern = '101000010010000000101'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended second augmented eleventh diminished thirteenth',
    code = 'maj7sus2#11b13',
    display = 'Maj7sus2#11b13',
    pattern = '101000010001000000101'
  },
  {
    complexity = 3,
    name = 'seventh suspended fourth augmented eleventh diminished thirteenth',
    code = '7sus4#11b13',
    display = '7sus4#11b13',
    pattern = '100001010010000000101'
  },
  {
    complexity = 3,
    name = 'Major seventh suspended fourth augmented eleventh diminished thirteenth',
    code = 'maj7sus4#11b13',
    display = 'Maj7sus4#11b13',
    pattern = '100001010001000000101'
  },
  {
    complexity = 3,
    name = 'seventh diminished fifth diminished ninth',
    code = '7b5b9',
    display = '7b5b9',
    pattern = '100010100010010'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished fifth diminished ninth',
    code = 'maj7b5b9',
    display = 'Maj7b5b9',
    pattern = '100010100001010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished fifth diminished ninth',
    code = 'min7b5b9',
    display = 'min7b5b9',
    pattern = '100100100010010'
  },
  { 
   complexity = 3,
    name = 'minor/Major seventh diminished fifth diminished ninth',
    code = 'm/maj7b5b9',
    display = 'm/Maj7b5b9',
    pattern = '100100100001010'
  },
  { 
   complexity = 3,
    name = 'minor seventh diminished fifth augmented ninth',
    code = 'min7b5#9',
    display = 'min7b5#9',
    pattern = '1001001000100001'
  },
  { 
    complexity = 3,
    name = 'minor/Major seventh diminished fifth augmented ninth',
    code = 'm/Maj7b5#9',
    display = 'm/Maj7b5#9',
    pattern = '1001001000010001'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished fifth augmented ninth diminished thirteenth',
    code = 'min7b5#9b13',
    display = 'min7b5#9b13',
    pattern = '1001001000100001000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished fifth augmented ninth diminished thirteenth',
    code = 'm/maj7b5#9b13',
    display = 'm/Maj7b5#9b13',
    pattern = '1001001000010001000010'
  },
  {
    complexity = 3,
    name = 'seventh diminished fifth diminished thirteenth',
    code = '7b5b13',
    display = '7b5b13',
    pattern = '1000101000010000000010'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished fifth diminished thirteenth',
    code = 'maj7b5b13',
    display = 'Maj7b5b13',
    pattern = '1000101000001000000010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished fifth diminished thirteenth',
    code = 'min7b5b13',
    display = 'min7b5b13',
    pattern = '1001001000100000000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished fifth diminished thirteenth',
    code = 'm/maj7b5b13',
    display = 'm/Maj7b5b13',
    pattern = '1001001000010000000010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished fifth augmented ninth',
    code = 'min7b5#9',
    display = 'min7b5#9',
    pattern = '1001001000100001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented fifth diminished ninth',
    code = 'min7#5b9',
    display = 'min7#5b9',
    pattern = '100100001010010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented fifth diminished ninth',
    code = 'm/maj7#5b9',
    display = 'm/Maj7#5b9',
    pattern = '100100001001010'
  },
  {
    complexity = 3,
    name = 'seventh augmented fifth diminished ninth',
    code = '7#5b9',
    display = '7#5b9',
    pattern = '100010001010010'
  },
  {
    complexity = 3,
    name = 'Major seventh augmented fifth diminished ninth',
    code = 'maj7#5b9',
    display = 'Maj7#5b9',
    pattern = '100010001001010'
  },
  {
    complexity = 3,
    name = 'seventh augmented fifth diminished ninth augmented eleventh',
    code = '7#5b9#11',
    display = '7#5b9#11',
    pattern = '1000100010100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh augmented fifth diminished ninth augmented eleventh',
    code = 'maj7#5b9#11',
    display = 'Maj7#5b9#11',
    pattern = '1000100010010100001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented fifth diminished ninth augmented eleventh',
    code = 'min7#5b9#11',
    display = 'min7#5b9#11',
    pattern = '1001000010100100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented fifth diminished ninth augmented eleventh',
    code = 'm/maj7#5b9#11',
    display = 'm/Maj7#5b9#11',
    pattern = '1001000010010100001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented fifth augmented ninth',
    code = 'min7#5#9',
    display = 'min7#5#9',
    pattern = '1001000010100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented fifth augmented ninth',
    code = 'm/Maj7#5#9',
    display = 'm/Maj7#5#9',
    pattern = '1001000010010001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented fifth augmented ninth augmented eleventh',
    code = 'min7#5#9#11',
    display = 'min7#5#9#11',
    pattern = '1001000010100001001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented fifth augmented ninth augmented eleventh',
    code = 'm/Maj7#5#9#11',
    display = 'm/Maj7#5#9#11',
    pattern = '1001000010010001001'
  },
  {
    complexity = 3,
    name = 'seventh augmented fifth augmented eleventh',
    code = '7#5#11',
    display = '7#5#11',
    pattern = '1000100010100000001'
  },
  {
    complexity = 3,
    name = 'Major seventh augmented fifth augmented eleventh',
    code = 'maj7#5#11',
    display = 'Maj7#5#11',
    pattern = '1000100010010000001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented fifth augmented eleventh',
    code = 'min7#5#11',
    display = 'min7#5#11',
    pattern = '1001000010100000001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented fifth augmented eleventh',
    code = 'm/Maj7#5#11',
    display = 'm/Maj7#5#11',
    pattern = '1001000010010000001'
  },
  {
    complexity = 2,
    name = 'seventh eight (octave)',
    code = '78',
    display = '78',
    pattern = '1000100100101'
  },
  {
    complexity = 2,
    name = 'Major seventh eight (octave)',
    code = 'maj78',
    display = 'Maj78',
    pattern = '1000100100011'
  },
  {
    complexity = 2,
    name = 'minor seventh eight (octave)',
    code = 'min78',
    display = 'min78',
    pattern = '1001000100101'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh eight (octave)',
    code = 'm/maj78',
    display = 'm/Maj78',
    pattern = '1001000100011'
  },
  {
    complexity = 1,
    name = 'seventh ninth',
    code = '79',
    display = '79',
    pattern = '100010010010001'
  },
  {
    complexity = 2,
    name = 'Major seventh ninth',
    code = 'maj79',
    display = 'Maj79',
    pattern = '100010010001001'
  },
  {
    complexity = 1,
    name = 'minor seventh ninth',
    code = 'min79',
    display = 'min79',
    pattern = '100100010010001'
  },
  {
    complexity = 2,
    name = 'minor/Major seventh ninth',
    code = 'm/maj79',
    display = 'm/Maj79',
    pattern = '100100010001001'
  },
  {
    complexity = 2,
    name = 'seventh diminished ninth',
    code = '7b9',
    display = '7b9',
    pattern = '100010010010010'
  },
  {
    complexity = 2,
    name = 'Major seventh diminished ninth',
    code = 'maj7b9',
    display = 'Maj7b9',
    pattern = '100010010001010'
  },
  {
    complexity = 2,
    name = 'minor seventh diminished ninth',
    code = 'min7b9',
    display = 'min7b9',
    pattern = '100100010010010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished ninth',
    code = 'm/maj7b9',
    display = 'm/Maj7b9',
    pattern = '100100010001010'
  },
  {
    complexity = 3,
    name = 'seventh diminished ninth augmented eleventh',
    code = '7b9#11',
    display = '7b9#11',
    pattern = '1000100100100100001'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished ninth augmented eleventh',
    code = 'maj7b9#11',
    display = 'Maj7b9#11',
    pattern = '1000100100010100001'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished ninth augmented eleventh',
    code = 'min7b9#11',
    display = 'min7b9#11',
    pattern = '1001000100100100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished ninth augmented eleventh',
    code = 'm/maj7b9#11',
    display = 'm/Maj7b9#11',
    pattern = '1001000100010100001'
  },
  {
    complexity = 3,
    name = 'seventh diminished ninth augmented eleventh diminished thirteenth',
    code = '7b9#11b13',
    display = '7b9#11b13',
    pattern = '1000100100100100001010'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished ninth augmented eleventh diminished thirteenth',
    code = 'maj7b9#11b13',
    display = 'Maj7b9#11b13',
    pattern = '1000100100010100001010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished ninth augmented eleventh diminished thirteenth',
    code = 'min7b9#11b13',
    display = 'min7b9#11b13',
    pattern = '1001000100100100001010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished ninth augmented eleventh diminished thirteenth',
    code = 'm/maj7b9#11b13',
    display = 'm/Maj7b9#11b13',
    pattern = '1001000100010100001010'
  },
  {
    complexity = 3,
    name = 'seventh diminished ninth diminished thirteenth',
    code = '7b9b13',
    display = '7b9b13',
    pattern = '1000100100100100000010'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished ninth diminished thirteenth',
    code = 'maj7b9b13',
    display = 'Maj7b9b13',
    pattern = '1000100100010100000010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished ninth diminished thirteenth',
    code = 'min7b9b13',
    display = 'min7b9b13',
    pattern = '1001000100100100000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished ninth diminished thirteenth',
    code = 'm/maj7b9b13',
    display = 'm/Maj7b9b13',
    pattern = '1001000100010100000010'
  },
  {
    complexity = 3,
    name = 'seventh diminished thirteenth',
    code = '7b13',
    display = '7b13',
    pattern = '1000100100100000000010'
  },
  {
    complexity = 3,
    name = 'Major seventh diminished thirteenth',
    code = 'maj7b13',
    display = 'Maj7b13',
    pattern = '1000100100010000000010'
  },
  {
    complexity = 3,
    name = 'minor seventh diminished thirteenth',
    code = 'min7b13',
    display = 'min7b13',
    pattern = '1001000100100000000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh diminished thirteenth',
    code = 'm/maj7b13',
    display = 'm/Maj7b13',
    pattern = '1001000100010000000010'
  },
  { 
    complexity = 2,
    name = 'minor seventh augmented ninth',
    code = 'min7#9',
    display = 'min7#9',
    pattern = '1001000100100001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented ninth',
    code = 'm/maj7#9',
    display = 'm/Maj7#9',
    pattern = '1001000100010001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented ninth augmented eleventh',
    code = 'min7#9#11',
    display = 'min7#9#11',
    pattern = '1001000100100001001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented ninth augmented eleventh',
    code = 'm/maj7#9#11',
    display = 'm/Maj7#9#11',
    pattern = '1001000100010001001'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented ninth augmented eleventh diminished thirteenth',
    code = 'min7#9#11b13',
    display = 'min7#9#11b13',
    pattern = '1001000100100001001010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented ninth augmented eleventh diminished thirteenth',
    code = 'm/maj7#9#11b13',
    display = 'm/Maj7#9#11b13',
    pattern = '1001000100010001001010'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented ninth diminished thirteenth',
    code = 'min7#9b13',
    display = 'min7#9b13',
    pattern = '1001000100100001000010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented ninth diminished thirteenth',
    code = 'm/maj7#9b13',
    display = 'm/Maj7#9b13',
    pattern = '1001000100010001000010'
  },
  {
    complexity = 2,
    name = 'seventh augmented eleventh',
    code = '7#11',
    display = '7#11',
    pattern = '1000100100100000001'
  },
  {
    complexity = 2,
    name = 'Major seventh augmented eleventh',
    code = 'maj7#11',
    display = 'Maj7#11',
    pattern = '1000100100010000001'
  },
  {
    complexity = 2,
    name = 'minor seventh augmented eleventh',
    code = 'min7#11',
    display = 'min7#11',
    pattern = '1001000100100000001'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented eleventh',
    code = 'm/maj7#11',
    display = 'm/Maj7#11',
    pattern = '1001000100010000001'
  },
  {
    complexity = 3,
    name = 'seventh augmented eleventh diminished thirteenth',
    code = '7#11b13',
    display = '7#11b13',
    pattern = '1000100100100000001010'
  },
  {
    complexity = 3,
    name = 'Major seventh augmented eleventh diminished thirteenth',
    code = 'maj7#11b13',
    display = 'Maj7#11b13',
    pattern = '1000100100010000001010'
  },
  {
    complexity = 3,
    name = 'minor seventh augmented eleventh diminished thirteenth',
    code = 'min7#11b13',
    display = 'min7#11b13',
    pattern = '1001000100100000001010'
  },
  {
    complexity = 3,
    name = 'minor/Major seventh augmented eleventh diminished thirteenth',
    code = 'm/maj7#11b13',
    display = 'm/Maj7#11b13',
    pattern = '1001000100010000001010'
  },
  {
    complexity = 0,
    name = 'Major eight (octave)',
    code = 'maj8',
    display = 'Maj8',
    pattern = '1000100100001'
  },
  {
    complexity = 0,
    name = 'minor eight (octave)',
    code = 'min8',
    display = 'min8',
    pattern = '1001000100001'
  },
  {
    complexity = 1,
    name = 'eight (octave) suspended second',
    code = '8sus2',
    display = '8sus2',
    pattern = '1010000100001'
  },
  {
    complexity = 1,
    name = 'eight (octave) suspended fourth',
    code = '8sus4',
    display = '8sus4',
    pattern = '1000010100001'
  },
  {
    complexity = 1,
    name = 'eight (octave) Power Chord',
    code = 'power8',
    display = '58',
    pattern = '1000000100001'
  },
  {
    complexity = 2,
    name = 'Major eight (octave) diminished fifth',
    code = 'maj8b5',
    display = 'Maj8b5',
    pattern = '1000101000001'
  },
  {
    complexity = 2,
    name = 'minor eight (octave) diminished fifth',
    code = 'min8b5',
    display = 'min8b5',
    pattern = '1001001000001'
  },
  {
    complexity = 2,
    name = 'Major eight (octave) augmented fifth',
    code = 'maj8#5',
    display = 'Maj8#5',
    pattern = '1000100010001'
  },
  {
    complexity = 2,
    name = 'minor eight (octave) augmented fifth',
    code = 'min8#5',
    display = 'min8#5',
    pattern = '1001000010001'
  },
  {
    complexity = 1,
    name = 'Major ninth',
    code = 'maj9',
    display = 'Maj9',
    pattern = '100010010000001'
  },
  {
    complexity = 1,
    name = 'minor ninth',
    code = 'min9',
    display = 'min9',
    pattern = '100100010000001'
  },
  {
    complexity = 3,
    name = 'Major ninth diminished fifth',
    code = 'maj9b5',
    display = 'Maj9b5',
    pattern = '100010100000001'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented fifth',
    code = 'min9#5',
    display = 'min9#5',
    pattern = '100100001000001'
  },
  {
    complexity = 3,
    name = 'Major ninth augmented eleventh',
    code = 'maj9#11',
    display = 'Maj9#11',
    pattern = '1000100100000010001'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented eleventh',
    code = 'min9#11',
    display = 'min9#11',
    pattern = '1001000100000010001'
  },
  {
    complexity = 3,
    name = 'Major ninth diminished fifth augmented eleventh',
    code = 'maj9b5#11',
    display = 'Maj9b5#11',
    pattern = '1000101000000010001'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented fifth augmented eleventh',
    code = 'min9#5#11',
    display = 'min9#5#11',
    pattern = '1001000010000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second',
    code = '9sus2',
    display = '9sus2',
    pattern = '101000010000001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth',
    code = '9sus4',
    display = '9sus4',
    pattern = '100001010000001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented fifth',
    code = '9sus2#5',
    display = '9sus2#5',
    pattern = '101000001000001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented fifth',
    code = '9sus4#5',
    display = '9sus4#5',
    pattern = '100001001000001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented fifth augmented eleventh',
    code = '9sus2#5#11',
    display = '9sus2#5#11',
    pattern = '1010000010000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented fifth augmented eleventh',
    code = '9sus4#5#11',
    display = '9sus4#5#11',
    pattern = '1000010010000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented eleventh',
    code = '9sus2#11',
    display = '9sus2#11',
    pattern = '1010000100000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented eleventh',
    code = '9sus4#11',
    display = '9sus4#11',
    pattern = '1000010100000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented eleventh diminished thirteenth',
    code = '9sus2#11b13',
    display = '9sus2#11b13',
    pattern = '1010000100000010001010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented eleventh diminished thirteenth',
    code = '9sus4#11b13',
    display = '9sus4#11b13',
    pattern = '1000010100000010001010'
  },
  {
    complexity = 3,
    name = 'ninth suspended second diminished thirteenth',
    code = '9sus2b13',
    display = '9sus2b13',
    pattern = '1010000100000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth diminished thirteenth',
    code = '9sus4b13',
    display = '9sus4b13',
    pattern = '1000010100000010000010'
  },
  {
    complexity = 3,
    name = 'Major ninth diminished fifth diminished thirteenth',
    code = 'maj9b5b13',
    display = 'Maj9b5b13',
    pattern = '1000101000000010000010'
  },
  {
    complexity = 3,
    name = 'minor ninth diminished fifth diminished thirteenth',
    code = 'min9b5b13',
    display = 'min9b5b13',
    pattern = '1001001000000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended second diminished fifth diminished thirteenth',
    code = '9sus2b5b13',
    display = '9sus2b5b13',
    pattern = '1010001000000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth diminished fifth diminished thirteenth',
    code = '9sus4b5b13',
    display = '9sus4b5b13',
    pattern = '1000011000000010000010'
  },
  {
    complexity = 3,
    name = 'Major ninth augmented fifth augmented eleventh',
    code = 'maj9#5#11',
    display = 'Maj9#5#11',
    pattern = '1000100010000010001'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented fifth augmented eleventh',
    code = 'min9#5#11',
    display = 'min9#5#11',
    pattern = '1001000010000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented fifth augmented eleventh',
    code = '9sus2#5#11',
    display = '9sus2#5#11',
    pattern = '1010000010000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented fifth augmented eleventh',
    code = '9sus4#5#11',
    display = '9sus4#5#11',
    pattern = '1000010010000010001'
  },
  {
    complexity = 3,
    name = 'Major ninth augmented fifth diminished thirteenth',
    code = 'maj9#5b13',
    display = 'Maj9#5b13',
    pattern = '1000100010000010000010'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented fifth diminished thirteenth',
    code = 'min9#5b13',
    display = 'min9#5b13',
    pattern = '1001000010000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented fifth diminished thirteenth',
    code = '9sus2#5b13',
    display = '9sus2#5b13',
    pattern = '1010000010000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented fifth diminished thirteenth',
    code = '9sus4#5b13',
    display = '9sus4#5b13',
    pattern = '1000010010000010000010'
  },
  {
    complexity = 2,
    name = 'Major ninth augmented eleventh',
    code = 'maj9#11',
    display = 'Maj9#11',
    pattern = '1000100100000010001'
  },
  {
    complexity = 2,
    name = 'minor ninth augmented eleventh',
    code = 'min9#11',
    display = 'min9#11',
    pattern = '1001000100000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented eleventh',
    code = '9sus2#11',
    display = '9sus2#11',
    pattern = '1010000100000010001'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented eleventh',
    code = '9sus4#11',
    display = '9sus4#11',
    pattern = '1000010100000010001'
  },
  {
    complexity = 3,
    name = 'Major ninth augmented eleventh diminished thirteenth',
    code = 'maj9#11b13',
    display = 'Maj9#11b13',
    pattern = '1000100100000010001010'
  },
  {
    complexity = 3,
    name = 'minor ninth augmented eleventh diminished thirteenth',
    code = 'min9#11b13',
    display = 'min9#11b13',
    pattern = '1001000100000010001010'
  },
  {
    complexity = 3,
    name = 'ninth suspended second augmented eleventh diminshed thirteenth',
    code = '9sus2#11b13',
    display = '9sus2#11b13',
    pattern = '1010000100000010001010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth augmented eleventh diminished thirteenth',
    code = '9sus4#11b13',
    display = '9sus4#11b13',
    pattern = '1000010100000010001010'
  },
  {
    complexity = 3,
    name = 'Major ninth diminished thirteenth',
    code = 'maj9b13',
    display = 'Maj9b13',
    pattern = '1000100100000010000010'
  },
  {
    complexity = 3,
    name = 'minor ninth diminished thirteenth',
    code = 'min9b13',
    display = 'min9b13',
    pattern = '1001000100000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended second diminshed thirteenth',
    code = '9sus2b13',
    display = '9sus2b13',
    pattern = '1010000100000010000010'
  },
  {
    complexity = 3,
    name = 'ninth suspended fourth diminished thirteenth',
    code = '9sus4b13',
    display = '9sus4b13',
    pattern = '1000010100000010000010'
  },
  {
    complexity = 0,
    name = 'Major tenth',
    code = 'maj10',
    display = 'Maj10',
    pattern = '10001001000000001'
  },
  {
    complexity = 0,
    name = 'minor tenth',
    code = 'min10',
    display = 'min10',
    pattern = '10010001000000010'
  },
  {
    complexity = 3,
    name = 'Major tenth diminished fifth',
    code = 'maj10b5',
    display = 'Maj10b5',
    pattern = '10001010000000001'
  },
  {
    complexity = 3,
    name = 'minor tenth augmented fifth',
    code = 'min10#5',
    display = 'min10#5',
    pattern = '10010000100000010'
  },
  {
    complexity = 2,
    name = 'Major tenth suspended fourth',
    code = 'maj10sus4',
    display = 'Maj10sus4',
    pattern = '10000101000000001'
  },
  {
    complexity = 2,
    name = 'minor tenth suspended fourth',
    code = 'min10sus4',
    display = 'min10sus4',
    pattern = '10000101000000010'
  },
  {
    complexity = 3,
    name = 'Major tenth suspended fourth augmented fifth',
    code = 'maj10sus4#5',
    display = 'Maj10sus4#5',
    pattern = '10000100100000001'
  },
  {
    complexity = 3,
    name = 'minor tenth suspended fourth augmented fifth',
    code = 'min10sus4#5',
    display = 'min10sus4#5',
    pattern = '10000100100000010'
  },
  {
    complexity = 0,
    name = 'eleventh suspended second',
    code = '11sus2',
    display = '11sus2',
    pattern = '101000010000000001'
  },
  {
    complexity = 1,
    name = 'eleventh suspended fourth',
    code = '11sus4',
    display = '11sus4',
    pattern = '100001010000000001'
  },
  {
    complexity = 3,
    name = 'eleventh suspended second diminished fifth',
    code = '11sus2b5',
    display = '11sus2b5',
    pattern = '101000100000000001'
  },
  {
    complexity = 3,
    name = 'eleventh suspended second augmented fifth',
    code = '11sus2#5',
    display = '11sus2#5',
    pattern = '101000001000000001'
  },
  {
    complexity = 3,
    name = 'eleventh suspended fourth augmented fifth',
    code = '11sus4#5',
    display = '11sus4#5',
    pattern = '100001001000000001'
  },
  {
    complexity = 0,
    name = 'Major twelfth',
    code = 'maj12',
    display = 'Maj12',
    pattern = '10001001000000000001'
  },
  {
    complexity = 0,
    name = 'minor twelfth',
    code = 'min12',
    display = 'min12',
    pattern = '10010001000000000001'
  },
  {
    complexity = 1,
    name = 'twelfth suspended second',
    code = '12sus2',
    display = '12sus2',
    pattern = '10100001000000000001'
  },
  {
    complexity = 1,
    name = 'twelfth suspended fourth',
    code = '12sus4',
    display = '12sus4',
    pattern = '10000101000000000001'
  }
}