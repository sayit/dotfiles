require 'scaleFunctions'
require 'ToolPreferences'
require 'interface'
require 'keyBindings'
require 'inversionStates'
require 'scaleDegreeHeaders'
require 'OSC'
require 'util'
require 'jumpLines'

scaleNotes = {}
scaleChords = {}

scalePattern = nil

chordButtons = {}
chordButtonPressed = false

function getNotesString(chordNotesArray)

  local notesString = '                '
  for i, note in ipairs(chordNotesArray) do
        
    local noteName = getNoteName(note+1)
    
    if i ~= #chordNotesArray then
      notesString = notesString .. noteName .. ', '
    else
      notesString = notesString .. noteName .. ''
    end
  end
  
  return notesString
end

function applyInversion(chord)

  local chord_ = chord  
  local chordLength = #chord_

  local chordInversionValue = getCurrentInversionValue()
  local oct = 0  

  if chordInversionValue < 0 then
    oct = math.floor(chordInversionValue / #chord_)
    chordInversionValue = chordInversionValue + (math.abs(oct) * #chord_)
  end
  
  for i = 1, chordInversionValue do
    local r = table.remove(chord_, 1)
    r = r + 12
    table.insert(chord_, #chord_ + 1, r )
  end
    
  for i = 1, #chord_ do
    chord_[i] = chord_[i] + (oct * 12)
  end

  return chord_
end

function effectsColumnIsSelected()
  return renoise.song().selected_note_column_index == 0
end

function insertNote(note, noteColumnIndex)

  if effectsColumnIsSelected() then
    return
  end
  
  if noteColumnIndex > renoise.song().selected_track.max_note_columns then
    return
  end
  
  -- Not enough space? Compensate!
  if noteColumnIndex > renoise.song().selected_track.visible_note_columns then
    renoise.song().selected_track.visible_note_columns = noteColumnIndex
  end
    
  local noteColumn = renoise.song().selected_line.note_columns[noteColumnIndex]
  
  if (note > -1 and note < 122) then
    noteColumn.note_value = note
  end
  
  if renoise.song().transport.keyboard_velocity_enabled then
    noteColumn.volume_value = renoise.song().transport.keyboard_velocity
  else
    noteColumn.volume_value = 255
  end
  noteColumn.instrument_value = renoise.song().selected_instrument_index - 1

  -- skip lines if checkbox is enabled
  if preferences.enableArpeggioCheckbox.value ~= false then
    local tmp_pos = renoise.song().transport.edit_pos
    tmp_pos.global_line_index = tmp_pos.global_line_index + renoise.song().transport.edit_step
    renoise.song().transport.edit_pos = tmp_pos

  -- insert noteoff if checkbox is enabled
    if (preferences.insertNoteOffInRemainingNoteColumns.value) and (renoise.song().transport.edit_step > 0) then
      local noteColumn = renoise.song().selected_line.note_columns[noteColumnIndex]
      noteColumn.note_value = 120
    end
  end
end

function insertChord(scaleDegree)
  
  preferences.selectedScaleDegree.value = scaleDegree
      
  local chordType = preferences.selectedChordTypes[scaleDegree].value
  if scaleChords[scaleDegree][chordType] ~= nil then
    local chord = scaleChords[scaleDegree][chordType]
    local root = scaleNotes[scaleDegree]

    local chordLength = 0
    local chordNotesArray = {}
    local chordPattern = chord["pattern"]

    for n = 0, #chordPattern-1 do
      if chordPattern:sub(n+1, n+1) == '1' then
        chordLength = chordLength + 1
        local octave = renoise.song().transport.octave
        local noteValue = root + n + (octave * 12) - 1
        table.insert(chordNotesArray, noteValue)
      end
    end
   
    chordNotesArray = applyInversion(chordNotesArray)
  
    local chordNotesArray_ = copy(chordNotesArray)

    if (preferences.arpeggioType.value == 2 or preferences.arpeggioType.value == 4) and preferences.enableArpeggioCheckbox.value == true then
      if preferences.addTriggerNoteCheckbox.value then
        table.insert(chordNotesArray, 1, preferences.triggerNote.value)
      end
    end
    if (preferences.arpeggioType.value == 1 or preferences.arpeggioType.value == 3) or preferences.enableArpeggioCheckbox.value == false then
      if preferences.addTriggerNoteCheckbox.value then
  
        local emptyNoteValue = 121
        if (preferences.insertNoteOffInRemainingNoteColumns.value) and (preferences.enableArpeggioCheckbox.value == false) then
          emptyNoteValue = 120
        end
    
        if preferences.enableArpeggioCheckbox.value == false then
          -- make it so the trigger note lines up when there are 2, 3 or 4 notes in the chord
          local numberOfColumnsForChord = 6
          for note = 1, numberOfColumnsForChord-#chordNotesArray do
            table.insert(chordNotesArray, emptyNoteValue)
          end
        end
    
        table.insert(chordNotesArray, preferences.triggerNote.value)
      end
    end

    if renoise.song().transport.edit_mode == true then
   
      local noteColumnIndex = renoise.song().selected_note_column_index
      if preferences.enableArpeggioCheckbox.value == true then

        if preferences.arpeggioType.value == 1 or preferences.arpeggioType.value == 2 then
          for note = 1, #chordNotesArray do
            insertNote(chordNotesArray[note], noteColumnIndex)
            noteColumnIndex = noteColumnIndex + 1
          end
        end
        if preferences.arpeggioType.value == 3 or preferences.arpeggioType.value == 4 then
          noteColumnIndex = #chordNotesArray
          for note = #chordNotesArray, 1, -1 do
            insertNote(chordNotesArray[note], noteColumnIndex)
            noteColumnIndex = noteColumnIndex - 1
          end
          noteColumnIndex = noteColumnIndex + 1
        end
      else
        for note = 1, #chordNotesArray do
          insertNote(chordNotesArray[note], noteColumnIndex)
          if (preferences.insertNoteOffInRemainingNoteColumns.value) then
            table.insert(chordNotesArray, 120)
          end
          noteColumnIndex = noteColumnIndex + 1
        end
      end
      local visibleNoteColumns = renoise.song().selected_track.visible_note_columns
          
      if preferences.enableArpeggioCheckbox.value == false then
        if visibleNoteColumns >= noteColumnIndex then
          for i = noteColumnIndex, visibleNoteColumns do
            local noteColumn = renoise.song().selected_line:note_column(i)
            
            if (preferences.insertNoteOffInRemainingNoteColumns.value) and ((preferences.enableArpeggioCheckbox.value == false) or (renoise.song().transport.edit_step == 0)) then
              noteColumn.note_value = 120
            else
              noteColumn.note_value = 121
            end
      
            noteColumn.instrument_value = 255
          end
        end
        local tmp_pos = renoise.song().transport.edit_pos
        tmp_pos.global_line_index = tmp_pos.global_line_index + renoise.song().transport.edit_step
        renoise.song().transport.edit_pos = tmp_pos
      end
    else
        playChordWithOsc(chordNotesArray)
    end
  
    updateChordText(root, chord, chordNotesArray_)
    
    highlightSelectedChordTypes()
  end
end
--------------------------------------------------------------------------------

function updateScaleNotes()

  scaleNotes = {}

  local scaleNoteIndex = 1
  for note = preferences.scaleTonicNote.value, preferences.scaleTonicNote.value + 11 do
  
    if noteIsInScale(note) then
      scaleNotes[scaleNoteIndex] = note
      scaleNoteIndex = scaleNoteIndex + 1
    end
  end
end

function updateScaleChords()

  scaleChords = {}

  local scaleNoteIndex = 1
  for note = preferences.scaleTonicNote.value, preferences.scaleTonicNote.value + 11 do
  
    if noteIsInScale(note) then
      scaleChords[scaleNoteIndex] = getScaleChordsForRootNote(note)
      scaleNoteIndex = scaleNoteIndex + 1
    end
  end
end

function updateChordButtons()

  local scaleNote = 1
  local theButtonHeight = ((preferences.buttonHeight * 3) + 15)  
  for note = preferences.scaleTonicNote.value, preferences.scaleTonicNote.value + 11 do
  
    if noteIsInScale(note) then
    
      local chordButtonsColumn = viewBuilder.views[string.format("%i", scaleNote)]
      if chordButtonsColumn == nil then 
        chordButtonsColumn = viewBuilder:column {
              id = "" .. scaleNote,
              margin = 0,
              spacing = 0,
        } 
      end
      
      chordButtons[scaleNote] = chordButtonsColumn       
      chordTypeColumns[scaleNote]:add_child(chordButtonsColumn)
    
      for chordIndex, chord in ipairs(chords) do
        
        local existingChordBox = viewBuilder.views[string.format("%i:%i", scaleNote, chordIndex)]
        
        if existingChordBox then
          existingChordBox.visible = false
        end
      end
      
      for chordIndex, chord in ipairs(scaleChords[scaleNote]) do
          
        local scaleNote_ = scaleNote
        local chordIndex_ = chordIndex
      
        local chordTypeButtonId = string.format("%i:%i", scaleNote, chordIndex)
        local chordTypeButton = viewBuilder.views[chordTypeButtonId]
          
        local buttonPressedNotifier = function()
                                        chordButtonPressed = true
                                        preferences.selectedChordTypes[scaleNote_].value = chordIndex_
                                        insertChord(scaleNote_)
                                        highlightSelectedChordTypes()
                                        preferences.chordInversion.value = getCurrentInversionValue()
                                        preferences:saveValues()
                                        chordButtonPressed = false
                                      end
          local buttonReleasedNotifier = function() 
      
        end
        
        if chordTypeButton then
          chordTypeButton.text = preferences.scaleNoteNames[scaleNote].value .. chord['display']
          chordTypeButton.visible = true
          chordTypeButton.width = BUTTON_WIDTH
          chordTypeButton.height = theButtonHeight
        else
          chordTypeButton = viewBuilder:button {
            id = chordTypeButtonId,
            color = {0x2d, 0x2d, 0x2d},
            height = theButtonHeight,            
            width = BUTTON_WIDTH,
            text = preferences.scaleNoteNames[scaleNote].value .. chord['display'],
            pressed = buttonPressedNotifier,
            released = buttonReleasedNotifier
          }
          chordButtonsColumn:add_child(chordTypeButton)
        end
      end
      scaleNote = scaleNote + 1
    end
  end
end


function clearChordBoxes()

  for i, v in ipairs(chordButtons) do
    if v ~= nil then
      chordTypeColumns[i]:remove_child(v)
      chordTypeColumns[i]:resize()
      chordButtons[i] = nil
    end 
  end
end

function removeFlatsAndSharps(arg)
  return arg:gsub('b',''):gsub('#','')
end

function aNoteIsRepeated()

 local previousScaleNoteName = preferences.scaleNoteNames[#preferences.scaleNoteNames].value
 local scaleNoteName = nil
 
  for scaleDegree = 1,  #preferences.scaleNoteNames do
  
    scaleNoteName = preferences.scaleNoteNames[scaleDegree].value

    if removeFlatsAndSharps(scaleNoteName) == removeFlatsAndSharps(previousScaleNoteName) then
      return true
    end
    
    previousScaleNoteName = scaleNoteName
  end
  
  return false
end

function updateScaleNoteNames()
  
  local previousScaleNoteName = getSharpNoteName(preferences.scaleTonicNote.value + 11)
  local scaleNoteName = nil
  
  local scaleDegree = 1
  for note = preferences.scaleTonicNote.value, preferences.scaleTonicNote.value + 11 do
  
    if scalePattern[getNotesIndex(note)] then

      scaleNoteName = getSharpNoteName(note)
      preferences.scaleNoteNames[scaleDegree].value = scaleNoteName      
      scaleDegree = scaleDegree + 1
      previousScaleNoteName = scaleNoteName
    end
  end
  
  if aNoteIsRepeated() then
  
    local previousScaleNoteName = getFlatNoteName(preferences.scaleTonicNote.value + 11)
    local scaleNoteName = nil
    
    local scaleDegree = 1
    for note = preferences.scaleTonicNote.value, preferences.scaleTonicNote.value + 11 do
    
      if scalePattern[getNotesIndex(note)] then
            
        scaleNoteName = getFlatNoteName(note)        
        preferences.scaleNoteNames[scaleDegree].value = scaleNoteName      
        scaleDegree = scaleDegree + 1
        previousScaleNoteName = scaleNoteName
      end
    end
  end
end

function updateScaleNotesText()
  
  local scaleNotesText = ''
  
  for i = 1, #scaleNotes do
    if scaleNotesText ~= '' then 
      scaleNotesText = scaleNotesText .. ', '
    end
    
    scaleNotesText = scaleNotesText .. preferences.scaleNoteNames[i].value
  end
  
  if viewBuilder ~= nil then
    viewBuilder.views.scale_notes.text = scaleNotesText
  end

  preferences.scaleNotesText.value = scaleNotesText
end

function getChordInversionText(chordNotesArray)

  local inversionValue = getCurrentInversionValue()
  
  if inversionValue == 0 then
    return ''
  end
  
  if math.fmod(inversionValue, #chordNotesArray) == 0 then
    return ''
  end
    
  return '/' .. getNoteName(chordNotesArray[1]+1)
end

function getChordInversionOctaveIndicator(numberOfChordNotes)

  local inversionValue = getCurrentInversionValue()

  local octaveIndicator = nil
   
  if inversionValue > 0 then
  
    local offsetValue = math.floor(inversionValue / numberOfChordNotes)
    
    if offsetValue > 0 then
      return '+' .. offsetValue
    else
      return '+'
    end
  
  elseif inversionValue < 0 then
  
    local offsetValue = math.abs(math.ceil(inversionValue / numberOfChordNotes))
    
    if offsetValue > 0 then
      return '-' .. offsetValue
    else  
      return '-'
    end
  else
    return ''
  end  
end

function updateChordText(root, chord, chordNotesArray)
  
  local rootNoteName = getNoteName(root)
  local chordInversionText = getChordInversionText(chordNotesArray)
  local chordInversionOctaveIndicator = getChordInversionOctaveIndicator(#chordNotesArray)
  local chordString = rootNoteName .. chord["display"]
  local chordLongName = rootNoteName .. ' ' .. chord["name"]
  local notesString = getNotesString(chordNotesArray)

  local chordTextValue = ''
  if string.match(chordInversionOctaveIndicator, "-") then
    chordTextValue = ("%20s%20s%s%20s"):format(chordInversionOctaveIndicator, chordString, chordInversionText, notesString)
  elseif string.match(chordInversionOctaveIndicator, "+") then
    chordTextValue = ("%20s%20s%s%20s%20s"):format('', chordString, chordInversionText, notesString, chordInversionOctaveIndicator)
  else
    chordTextValue = ("%20s%20s%s%20s"):format('', chordString, chordInversionText, notesString)
  end

  preferences.chordLongName.value = chordLongName  
  preferences.chordText.value = chordTextValue
  
  if (viewBuilder ~= nil) then
    viewBuilder.views.chord_text.text = chordTextValue
    viewBuilder.views.chord_longname.text = chordLongName
  end
  
  renoise.app():show_status(chordTextValue)
end

function updateScaleData()
  scalePattern = getScalePattern(preferences.scaleTonicNote.value, scales[preferences.scaleType.value])
  updateScaleNotes()
  updateScaleNoteNames()
  updateScaleNotesText()
  updateScaleChords()
  
  if (viewBuilder ~= nil) then
    updateChordButtons()
    highlightSelectedChordTypes()
  end
end

function showScaleStatus()

  local scaleTonicText =  notes[preferences.scaleTonicNote.value]
  local scaleTypeText = scales[preferences.scaleType.value].name
  local scaleNotesText = preferences.scaleNotesText.value
  renoise.app():show_status(("%s %s: %s"):format(scaleTonicText, scaleTypeText, scaleNotesText))
end

local dialog = nil

renoise.tool():add_menu_entry {
  name = "Main Menu:Tools:ChordLord 3.06.235 (235 Chords)", 
  invoke = function()
  
    if dialog ~= nil and dialog.visible then
      dialog:show()
      return
    end

    viewBuilder = renoise.ViewBuilder()
    preferences = ToolPreferences()
    preferences:loadValues()
    
    local dialogView = getDialogView()
    dialog = renoise.app():show_custom_dialog('ChordLord 3.06.235 (235 chords)', dialogView, keyHandler)
    updateScaleData()
    highlightSelectedChordTypes()
    
    if oscClient == nil then
      connectOSC()
    end
  end
}
