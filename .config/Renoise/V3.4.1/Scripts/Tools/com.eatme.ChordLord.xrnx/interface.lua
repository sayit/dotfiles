require 'ToolPreferences'
require 'scaleFunctions'
arpeggioNames = {"up + extra", "extra + up", "extra + down", "down + extra" }

BUTTON_WIDTH = 135

viewBuilder = nil

chordTypeColumns = {}

scaleNames = {}
for key, scale in ipairs(scales) do
  table.insert(scaleNames, scale['name'])
end

function setInversion(value)

  local selectedScaleDegree = preferences.selectedScaleDegree.value
  local selectedChordTypeIndex = preferences.selectedChordTypes[selectedScaleDegree].value

  setInversionState(selectedScaleDegree, selectedChordTypeIndex, value)
  if (chordButtonPressed == false) then
    insertChord(selectedScaleDegree)
  end
end

function highlightSelectedChordTypes()

  if viewBuilder == nil then
    return
  end
  
  for i = 1, #preferences.selectedChordTypes do
  
    if scaleChords[i] then
    
      for j = 1, #scaleChords[i] do
        
        local chordTypeBox = viewBuilder.views[string.format("%i:%i", i, j)]

        if j == preferences.selectedChordTypes[i].value then
        
          if i == preferences.selectedScaleDegree.value then
            viewBuilder.views[string.format("%i:%i", i, j)].color = {0xdc, 0xdc, 0xdc}
          else
            viewBuilder.views[string.format("%i:%i", i, j)].color = {0x47, 0x47, 0x47}
          end
        else
          viewBuilder.views[string.format("%i:%i", i, j)].color = {0x2d, 0x2d, 0x2d}
        end
      end
    end
  end
end

function getDialogView ()
  for i = 1, 7 do
    chordTypeColumns[i] = viewBuilder:column {
      style = 'panel', 
      viewBuilder:text{
        id = 'scaleDegreeHeader' .. i,
        align = 'center',
        width = BUTTON_WIDTH,
        font = 'bold', 
        text = preferences.scaleDegreeHeaders[i].value
      }
    }
  end
  
  return viewBuilder:column {
 
          margin = renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN,
          
          viewBuilder:row {
            
            spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING*3,
            
            viewBuilder:column {
              spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING,
              
              viewBuilder:horizontal_aligner {
              
                spacing = 1,  
                viewBuilder:row {
                
                  id = "main_row",
                  spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING,
                  margin = renoise.ViewBuilder.DEFAULT_CONTROL_MARGIN,
                  style = 'group',
                  
                  viewBuilder:text { text = 'Scale:' },
                  
                  viewBuilder:popup { 
                    id = "scale_tonic",
                    items = notes,
                    bind = preferences.scaleTonicNote,
                    width = 50,
                    notifier = function (i)
                        chordButtonPressed = true  
                        clearChordBoxes()
                        preferences.scaleTonicNote.value = i
                        preferences:resetSelectedChordTypes()
                        preferences:resetSelectedInversionStates()
                        updateScaleData()
                        preferences:saveValues()
                        showScaleStatus()
                        updateScaleDegreeHeaders()
                        chordButtonPressed = false
                    end
                  },
  
                  viewBuilder:popup { 
                    id = "scale",
                    width = 183,
                    items = scaleNames,
                    bind = preferences.scaleType,
                    notifier = function (i) 
                        chordButtonPressed = true  
                        clearChordBoxes()
                        preferences.scaleType.value = i
                        preferences:resetSelectedChordTypes()
                        preferences:resetSelectedInversionStates()
                        updateScaleData()              
                        preferences:saveValues()
                        showScaleStatus()
                        updateScaleDegreeHeaders()
                        chordButtonPressed = false
                    end  
                  },
                  
                  viewBuilder:space { width = 5 },
                  viewBuilder:text{
                    id = 'scale_notes',
                    align = 'center',
                    width = 183,
                    font = 'bold',
                    text = preferences.scaleNotesText.value
                  },
                },
                viewBuilder:button {
                  id= 'jump',
                  height= '100%',
                  text = 'jump',
                  pressed = function()

                    local tmp_pos = renoise.song().transport.edit_pos
                    tmp_pos.global_line_index = tmp_pos.global_line_index + renoise.song().transport.edit_step
                    renoise.song().transport.edit_pos = tmp_pos
                  end
                },
                viewBuilder:button {
                  id= 'jumpup',
                  height= '100%',
                  text = 'jump up',
                  pressed = function()

                    local tmp_pos = renoise.song().transport.edit_pos
                    if tmp_pos.global_line_index > renoise.song().transport.edit_step then
                      tmp_pos.global_line_index = tmp_pos.global_line_index - renoise.song().transport.edit_step
                    else
                      tmp_pos.global_line_index = 1
                    end
                    renoise.song().transport.edit_pos = tmp_pos
                  end
                },
                viewBuilder:button { 
                  id = "options",
                  height = "100%",
                  text = "options",
                  pressed = function()
                  
                    if preferences.optionsPanelIsVisible.value then
                      viewBuilder.views.options_row.visible = false
                      preferences.optionsPanelIsVisible.value = false
                    else 
                      viewBuilder.views.options_row.visible = true
                      preferences.optionsPanelIsVisible.value = true
                    end
                    
                    preferences:saveValues()
                  end 
                  },
                },
                viewBuilder:column {
                
                  id = "chord_view",
                  style = 'group',
                  margin = renoise.ViewBuilder.DEFAULT_CONTROL_MARGIN,
                  spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING,
                  height = 200,
                  
                  viewBuilder:horizontal_aligner {

                  
                      mode = "justify",
                      
                      viewBuilder:column {
                        viewBuilder:horizontal_aligner {
                          viewBuilder:text { text = "Scroll: " },
                          viewBuilder:valuebox {
                            id = "chord_scroll",
                            bind = preferences.chordScroll,
                            min = preferences.chordScrollMin.value,
                            max = preferences.chordScrollMax.value, 
                            notifier = function(value)
                              chordButtonPressed = true
                              clearChordBoxes()
                              preferences:resetSelectedChordTypes()
                              preferences:resetSelectedInversionStates()
                              updateScaleData()              
                              preferences:saveValues()
                              showScaleStatus()
                              updateScaleDegreeHeaders()
                              chordButtonPressed = false
                            end
                          },
                          viewBuilder:text { text = "Window size: " },
                          viewBuilder:valuebox {
                            id = "chord_maxdisplay",
                            bind = preferences.chordMaxDisplay,
                            min = 15,
                            max = 45,
                            notifier = function(value)
                              chordButtonPressed = true
                              clearChordBoxes()
                              preferences:resetSelectedChordTypes()
                              preferences:resetSelectedInversionStates()
                              updateScaleData()              
                              preferences:saveValues()
                              showScaleStatus()
                              updateScaleDegreeHeaders()
                              chordButtonPressed = false
                            end
                          },
                          viewBuilder:text { text = "Button size: " },
                          viewBuilder:valuebox {
                            id = "button_size",
                            bind = preferences.buttonHeight,
                            min = 1,
                            max = 15,
                            notifier = function(value)
                              chordButtonPressed = true
                              clearChordBoxes()
                              preferences:resetSelectedChordTypes()
                              preferences:resetSelectedInversionStates()
                              updateScaleData()              
                              preferences:saveValues()
                              showScaleStatus()
                              updateScaleDegreeHeaders()
                              chordButtonPressed = false
                            end
                          },
                          viewBuilder:text { text = "Chord complexity: " },
                          viewBuilder:valuebox {
                            id = "chord_complexitylevel",
                            bind = preferences.chordComplexityLevel,
                            min = 0,
                            max = 3,
                            notifier = function(value)
                              chordButtonPressed = true
                              clearChordBoxes()
                              preferences:resetSelectedChordTypes()
                              preferences:resetSelectedInversionStates()
                              updateScaleData()              
                              preferences:saveValues()
                              showScaleStatus()
                              updateScaleDegreeHeaders()
                              chordButtonPressed = false
                            end
                          },
                        },
                      },
                      viewBuilder:column {
                          viewBuilder:text{
                            id = 'chord_text',
                            width = 160,
                            text = preferences.chordText.value,
                            font = 'bold',
                          }, 
                      },
                      
                      viewBuilder:column {
                        viewBuilder:horizontal_aligner {
                          viewBuilder:text { text = "Inversion: " },
                          viewBuilder:valuebox {
                            id = "chord_inversion",
                            bind = preferences.chordInversion,
                            min = preferences.chordInversionMin.value,
                            max = preferences.chordInversionMax.value, 
                            notifier = function(value)
                              setInversion(value)
                              preferences:saveValues()
                            end
                          },
                        },
                      },
                   },
                    viewBuilder:row{
                      viewBuilder:column {
                        viewBuilder:text{
                          width=120,
                          text = '',
                        },
                      },
                      viewBuilder:column {
                        viewBuilder:text{
                          id = 'chord_longname',
                          width = 600,
                          text = preferences.chordLongName.value,
                          font = 'bold',
                        }, 
                      },
                    },
                    viewBuilder:row{
                    margin = 0,
                    spacing = 0,
                      chordTypeColumns[1], 
                      chordTypeColumns[2], 
                      chordTypeColumns[3], 
                      chordTypeColumns[4], 
                      chordTypeColumns[5], 
                      chordTypeColumns[6], 
                      chordTypeColumns[7]
                    },
                    viewBuilder:row{
                    margin = 0,
                    spacing = 0,
                      viewBuilder:text {
                        text = " Go scroll on top! "
                      },
                    },
                  },
                },
              viewBuilder:row { 
                id = "options_row",
                style = 'group',
                width = 700,
                spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING, 
                margin = renoise.ViewBuilder.DEFAULT_CONTROL_MARGIN,
                visible = preferences.optionsPanelIsVisible.value,
                
                viewBuilder:vertical_aligner {
  
                  viewBuilder:horizontal_aligner {
    
                    viewBuilder:text {
                        font = 'bold', 
                        text = "options:"
                    }
                  },  
            
                  viewBuilder:horizontal_aligner {
                  
                      mode = "left",
                      viewBuilder:row {
                        viewBuilder:checkbox {
                          id = "insert_note_offs",
                          bind = preferences.insertNoteOffInRemainingNoteColumns,
                          notifier = function()
                            chordButtonPressed = true
                            preferences:saveValues()
                            chordButtonPressed = false
                          end
                        },
                        viewBuilder:text { text = "insert note offs" }
                    },
                  },
                  
                  viewBuilder:horizontal_aligner {
                  
                      mode = "left",
                      viewBuilder:row {
                        viewBuilder:checkbox {
                          id = "enable_modal_mixture",
                          bind = preferences.enableModalMixtureCheckbox,
                          notifier = function()
                            chordButtonPressed = true
                            if preferences.enableModalMixtureCheckbox.value == true then
                              viewBuilder.views.modal_mixture_scale.active = true
                            else
                              viewBuilder.views.modal_mixture_scale.active = false
                            end
                            
                            clearChordBoxes()
                            preferences:resetSelectedChordTypes()
                            preferences:resetSelectedInversionStates()
                            updateScaleData()
                            preferences:saveValues()
                            chordButtonPressed = false
                          end
                        },
                        viewBuilder:text { text = "modal mixture" }
                    },
                  },
                  viewBuilder:popup { 
                    id = "modal_mixture_scale",
                    items = scaleNames,
                    active = preferences.enableModalMixtureCheckbox.value,
                    width = 120,
                    bind = preferences.modalMixtureScaleType,
                    notifier = function (i) 
                      chordButtonPressed = true  
                      clearChordBoxes()
                      preferences:resetSelectedChordTypes()
                      preferences:resetSelectedInversionStates()
                      updateScaleData()
                      preferences:saveValues()
                      chordButtonPressed = false
                    end  
                  },
                  
                  viewBuilder:horizontal_aligner {
                  
                      mode = "left",
                      viewBuilder:row {
                        viewBuilder:checkbox {
                          bind = preferences.enableAllChordsCheckbox,
                          notifier = function()
                            chordButtonPressed = true
                            clearChordBoxes()
                            preferences:resetSelectedChordTypes()
                            preferences:resetSelectedInversionStates()
                            updateScaleData()
                            preferences:saveValues()
                            chordButtonPressed = false
                          end
                        },
                        viewBuilder:text { text = "all chords" }
                    },
                  },
                  
                  viewBuilder:horizontal_aligner {
                  
                      mode = "left",
                      viewBuilder:row {
                        viewBuilder:checkbox {
                          id = "trigger_note_checkbox",
                          bind = preferences.addTriggerNoteCheckbox,
                          notifier = function()
                          
                            preferences:saveValues()
                          
                            if preferences.addTriggerNoteCheckbox.value == true then
                              viewBuilder.views.trigger_note.active = true
                            else
                              viewBuilder.views.trigger_note.active = false
                            end
                            
                          end
                        },
                        viewBuilder:text { text = "add extra note" }
                    },
                  },
                  
                  viewBuilder:horizontal_aligner {
                 
                    mode = "left",
                    viewBuilder:valuebox {
                                          id = 'trigger_note',
                                          active = preferences.addTriggerNoteCheckbox.value,
                                          width = 80,
                                          min = 0,
                                          max = 119,
                                          bind = preferences.triggerNote,
                                          notifier = function(newValue)

                                          end,
                                          tostring = function(value)
                                            return getSharpNoteName(value+1) .. getOctave(value)
                                          end,
                                          tonumber = function(string) 
                                            return tonumber(string)
                                          end
                                         }
                   },

                   viewBuilder:horizontal_aligner {
                  
                      mode = "left",
                      viewBuilder:row {
                        viewBuilder:checkbox {
                          id = "enable_arpeggio",
                          bind = preferences.enableArpeggioCheckbox,
                          notifier = function()
                            if preferences.enableArpeggioCheckbox.value == true then
                              viewBuilder.views.arpeggio_type.active = true
                            else
                              viewBuilder.views.arpeggio_type.active = false
                            end
                            preferences:saveValues()
                          end
                        },
                        viewBuilder:text { text = "arpeggio (skip lines)" }
                     },
                   },
                   viewBuilder:popup { 
                     id = "arpeggio_type",
                     items = arpeggioNames,
                     active = preferences.enableArpeggioCheckbox.value,
                     width = 120,
                     bind = preferences.arpeggioType,
                     notifier = function (i) 
                       preferences:saveValues()
                     end  
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "Record on: clicked chords to pattern." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "  Press [Esc] in the Pattern Editor." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "[Numpad Enter] to loop in the pattern." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "OSC Server on: hear clicked chords." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "  see menu Edit.. Preferences.. OSC." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "Set Keys: menu Edit.. Preferences.. " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "  at Keys, search for ChordLord." }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "Developed by" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " EatMe" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " Panda" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " Suva" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = " " }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { text = "Special thanks to" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " Pone" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " Joule" }
                   },
                   viewBuilder:horizontal_aligner {
                     mode = "left",
                     viewBuilder:text { font='bold', text = " Neuro... No Neuro" }
                   },


                  --[[]]--
              }
            }
          }
        }
end
