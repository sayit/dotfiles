require 'chords'

defaultOptionsPanelIsVisibleValue = false
defaultInsertNoteOffInRemainingNoteColumnsValue = false
defaultScaleTonicNoteValue = 1
defaultScaleTypeValue = 1
defaultChordLongName = ''
defaultScaleNotesTextValue = ''
defaultChordTextValue = ''
defaultChordInversion = 0
defaultChordInversionMin = -8
defaultChordInversionMax = 8
defaultSelectedScaleDegree = 1
defaultChordComplexityLevel = 3
defaultArpeggioType = 1

defaultSelectedChordTypes = {}
for i = 1, 7 do
  table.insert(defaultSelectedChordTypes, 1)
end

defaultSelectedInversionStates = {}
for i = 1, #chords*2 do
  table.insert(defaultSelectedInversionStates, 0)
end

defaultTriggerNoteValue = 12
defaultAddTriggerNoteCheckboxValue = false
defaultEnableModalMixtureCheckboxValue = false
defaultModalMixtureScaleTypeValue = 2
defaultEnableAllChordsCheckboxValue = false
defaultEnableArpeggioCheckboxValue = false