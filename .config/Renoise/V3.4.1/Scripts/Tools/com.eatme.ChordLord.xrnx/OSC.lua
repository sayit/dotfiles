oscClient = nil
socketError = nil

function connectOSC()
  oscClient, socketError = renoise.Socket.create_client("localhost", 8000, renoise.Socket.PROTOCOL_UDP)
   
  if socketError then 
    renoise.app():show_warning(("failed to start the OSC client: '%s'"):format(socketError))
  end
end

function getVelocityValue()

  if renoise.song().transport.keyboard_velocity_enabled then
    return renoise.song().transport.keyboard_velocity
  else
    return 128
  end
end

function stopPreviousChord(chordNotesArray)

  local oscMessages = {}
  
  for i = 1, #chordNotesArray do
    
    local instrumentIndexTag = {tag="i", value = renoise.song().selected_instrument_index}   
    local masterTrackIndex = renoise.song().selected_track_index
    local trackIndexTag = {tag="i", value = masterTrackIndex}
    local noteValueTag = {tag="i", value = chordNotesArray[i]}
 
    local tags = {}
    table.insert(tags, instrumentIndexTag)
    table.insert(tags, trackIndexTag)
    table.insert(tags, noteValueTag)
     
    local oscMessage = renoise.Osc.Message("/renoise/trigger/note_off", tags)
    
    table.insert(oscMessages, oscMessage)             
  end

  local oscBundle = renoise.Osc.Bundle(os.clock(), oscMessages)
  oscClient:send(oscBundle)
end

previousChordNotesArray = nil

function playChordWithOsc(chordNotesArray)

  if oscClient == nil then
    connectOSC()
  end

  if previousChordNotesArray ~= nil then
    stopPreviousChord(previousChordNotesArray)
  end

  local oscMessages = {}
  
  for i = 1, #chordNotesArray do
    
    local velocityValue = getVelocityValue()
  
    local instrumentIndexTag = {tag="i", value = renoise.song().selected_instrument_index}   
    local masterTrackIndex = renoise.song().selected_track_index
    local trackIndexTag = {tag="i", value = masterTrackIndex}
    local noteValueTag = {tag="i", value = chordNotesArray[i]}
    local velocityValueTag = {tag="i", value = velocityValue}
 
    local tags = {}
    table.insert(tags, instrumentIndexTag)
    table.insert(tags, trackIndexTag)
    table.insert(tags, noteValueTag)
    table.insert(tags, velocityValueTag)
     
    local oscMessage = renoise.Osc.Message("/renoise/trigger/note_on", tags)
    table.insert(oscMessages, oscMessage)
  end
  
  local oscBundle = renoise.Osc.Bundle(os.clock(), oscMessages)
  oscClient:send(oscBundle)
  
  previousChordNotesArray = chordNotesArray
end
