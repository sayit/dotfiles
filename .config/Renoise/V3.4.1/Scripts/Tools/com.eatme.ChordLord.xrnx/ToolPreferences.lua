require 'defaultValues'
require 'scaleFunctions'
require 'inversionStates'

MAXCHORDSCROLL = 340
preferences = nil

class 'ToolPreferences'(renoise.Document.DocumentNode)

function ToolPreferences:__init()

  renoise.Document.DocumentNode.__init(self)

  self:add_property("optionsPanelIsVisible", defaultOptionsPanelIsVisibleValue)
  self:add_property("insertNoteOffInRemainingNoteColumns", defaultInsertNoteOffInRemainingNoteColumnsValue)
  self:add_property("scaleTonicNote", defaultScaleTonicNoteValue)
  self:add_property("chordLongName", defaultChordLongName)
  self:add_property("chordText", defaultChordTextValue)
  self:add_property("scaleType", defaultScaleTypeValue)
  self:add_property("scaleNoteNames", {'C', 'D', 'E', 'F', 'G', 'A', 'B'})
  self:add_property("scaleNotesText", defaultScaleNotesTextValue)
  self:add_property("scaleDegreeHeaders", {'I', 'ii', 'iii', 'IV', 'V', 'vi', 'vii°'})

  self:add_property("chordInversion", defaultChordInversion)
  self:add_property("chordInversionMin", defaultChordInversionMin)
  self:add_property("chordInversionMax", defaultChordInversionMax)
  
  self:add_property("selectedScaleDegree", defaultSelectedScaleDegree)

  self:add_property("chordComplexityLevel", defaultChordComplexityLevel)
   
  self:add_property("selectedChordTypes", defaultSelectedChordTypes)

  self:add_property("chordScroll", 0)
  self:add_property("chordScrollMin", 0)
  self:add_property("chordScrollMax", MAXCHORDSCROLL)
  self:add_property("chordMaxDisplay", 20)
  self:add_property("buttonHeight", 5)
 
  self:add_property("selectedInversionStates1", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates2", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates3", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates4", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates5", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates6", defaultSelectedInversionStates)
  self:add_property("selectedInversionStates7", defaultSelectedInversionStates)
  
  self:add_property("addTriggerNoteCheckbox", defaultAddTriggerNoteCheckboxValue)
  self:add_property("triggerNote", defaultTriggerNoteValue)
  self:add_property("enableModalMixtureCheckbox", defaultEnableModalMixtureCheckboxValue)
  self:add_property("modalMixtureScaleType", defaultModalMixtureScaleTypeValue)
  self:add_property("enableAllChordsCheckbox", defaultEnableAllChordsCheckboxValue)
  self:add_property("enableArpeggioCheckbox", defaultEnableArpeggioCheckboxValue)
  self:add_property("arpeggioType", defaultArpeggioType)
end

function ToolPreferences:loadValues()
  preferences:load_from("preferences.xml")
  preferences.chordScrollMax.value = MAXCHORDSCROLL
end

function ToolPreferences:saveValues()
  preferences.chordInversion.value = getCurrentInversionValue()
  preferences.chordScrollMax.value = MAXCHORDSCROLL
  self:save_as("preferences.xml")
end

function ToolPreferences:resetSelectedChordTypes()

  for i = 1, #self.selectedChordTypes do
    self.selectedChordTypes[i].value = 1
  end
end

function ToolPreferences:resetSelectedInversionStates()

  for i = 1, #self.selectedInversionStates1 do
    self.selectedInversionStates1[i].value = 0
  end

  for i = 1, #self.selectedInversionStates2 do
    self.selectedInversionStates2[i].value = 0
  end
  
  for i = 1, #self.selectedInversionStates3 do
    self.selectedInversionStates3[i].value = 0
  end

  for i = 1, #self.selectedInversionStates4 do
    self.selectedInversionStates4[i].value = 0
  end
  
  for i = 1, #self.selectedInversionStates5 do
    self.selectedInversionStates5[i].value = 0
  end

  for i = 1, #self.selectedInversionStates6 do
    self.selectedInversionStates6[i].value = 0
  end
  
  for i = 1, #self.selectedInversionStates7 do
    self.selectedInversionStates7[i].value = 0
  end
  
  self.chordInversion.value = 0
end
