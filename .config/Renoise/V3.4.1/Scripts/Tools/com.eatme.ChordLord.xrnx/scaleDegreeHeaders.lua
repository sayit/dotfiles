function updateScaleDegreeHeaders()

  local minorSymbols = {'i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii'}
  local majorSymbols = {'I', 'II', 'III', 'IV', 'V', 'VI', 'VII'}
  local diminishedSymbol = '°'
  local augmentedSymbol = '⁺'
  local sixthSymbol = '⁶'
  local seventhSymbol = '⁷'
  
  local i = 1
  for i = 1, 7 do
  
    local symbol = ''
  
    if i > #scaleNotes then
      preferences.scaleDegreeHeaders[i].value = symbol
      
    else
      if scaleChords[i][1] ~= nil then
      
        local chord = scaleChords[i][1]
      
        if string.match(chord.code, "maj") or chord.code == '7' then
          symbol = majorSymbols[i]
        else
          symbol = minorSymbols[i]
        end
      
        if (chord.code == 'aug') then
          symbol = symbol .. augmentedSymbol
        end
  
        if (chord.code == 'dim') then
          symbol = symbol .. diminishedSymbol
        end
  
        if string.match(chord.code, "6") then
          symbol = symbol .. sixthSymbol
        end
      
        if string.match(chord.code, "7") then
          symbol = symbol .. seventhSymbol
        end
          
        preferences.scaleDegreeHeaders[i].value = symbol    
      end
    end
    if (viewBuilder ~= nil) then
      
      if (i == 1) then
        viewBuilder.views.scaleDegreeHeader1.text = symbol
      end
  
      if (i == 2) then
        viewBuilder.views.scaleDegreeHeader2.text = symbol
      end
      
      if (i == 3) then
        viewBuilder.views.scaleDegreeHeader3.text = symbol
      end
      
      if (i == 4) then
        viewBuilder.views.scaleDegreeHeader4.text = symbol
      end
      
      if (i == 5) then
        viewBuilder.views.scaleDegreeHeader5.text = symbol
      end
      
      if (i == 6) then
        viewBuilder.views.scaleDegreeHeader6.text = symbol
      end                
  
      if (i == 7) then
        viewBuilder.views.scaleDegreeHeader7.text = symbol
      end    
    end
  end
end
