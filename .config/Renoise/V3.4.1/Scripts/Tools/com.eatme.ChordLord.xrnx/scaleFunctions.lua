require 'scales'
require 'chords'

notes = { 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' };
flatNotes = { 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B' };

function getScalePattern(scaleTonicNote, scale)

  local scalePatternString = scale['pattern']
  local scalePattern = {false,false,false,false,false,false,false,false,false,false,false}

  for i = 0, #scalePatternString do
    local note = getNotesIndex(scaleTonicNote+i)
    if scalePatternString:sub(i+1, i+1) == '1' then
      scalePattern[note] = true
    end
  end
  return scalePattern
end

function getNotesIndex(note) 
   return ((note - 1) % 12) + 1
end

function getNoteName(note)

  local noteName = getSharpNoteName(note)
  
  if not string.match(preferences.scaleNotesText.value, noteName) then
    return getFlatNoteName(note)
  else
    return noteName
  end
end

function getSharpNoteName(note)
  local notesIndex = getNotesIndex(note)
  return notes[notesIndex]
end

function getFlatNoteName(note)
  local notesIndex = getNotesIndex(note)
  return flatNotes[notesIndex]
end

function getOctave(noteValue)
  return math.floor(noteValue / 12)
end

function chordIsNotAlreadyIncluded(scaleChordsForRootNote, chordCode)

  for chordIndex, chord in ipairs(scaleChordsForRootNote) do
  
    if chord.code == chordCode then
      return false
    end
  end
  
  return true
end

function getScaleChordsForRootNote(rootNote)
  
  local chordCount = -3
  local chordStartDisplay = 0
  local chordMaxDisplay = 2
  local scaleChordsForRootNote = {}
  local bRebuildScreen = true
  local iScreenRebuild = 0
  
  chordStartDisplay = preferences.chordScroll.value
  chordMaxDisplay = preferences.chordMaxDisplay.value + 1
  
  for iScreenRebuild=1, 2 do
    if bRebuildScreen ~= false then
      for chordIndex, chord in ipairs(chords) do
        
        if chordIsInScale(rootNote, chordIndex) then
          if chord.complexity <= preferences.chordComplexityLevel.value then
            if chordCount == -3 then
              chordCount = 0
            end
            chordCount = chordCount + 1
            if chordCount < chordMaxDisplay then
              if chordCount > chordStartDisplay then
                scaleChordsForRootNote[chordCount] = chord   
              else 
                chordCount = chordCount - 1
                chordStartDisplay = chordStartDisplay - 1
              end
            end
          end
        end
      end
       
      if preferences.enableModalMixtureCheckbox.value then
  
        for chordIndex, chord in ipairs(chords) do
               
          if chordIsNotAlreadyIncluded(scaleChordsForRootNote, chord.code) and chordIsInModalMixtureScale(rootNote, chordIndex) then
            if chord.complexity <= preferences.chordComplexityLevel.value then
              if chordCount == -3 then
                chordCount = 0
              end
              chordCount = chordCount + 1
              if chordCount < chordMaxDisplay then
                if chordCount > chordStartDisplay then
                  scaleChordsForRootNote[chordCount] = chord
                else 
                  chordCount = chordCount - 1
                  chordStartDisplay = chordStartDisplay - 1
                end
              end
            end
          end
        end
      end
    
      if preferences.enableAllChordsCheckbox.value then

        for chordIndex, chord in ipairs(chords) do
             
          if chordIsNotAlreadyIncluded(scaleChordsForRootNote, chord.code) then
            if chord.complexity <= preferences.chordComplexityLevel.value then
              if chordCount == -3 then
                chordCount = 0
              end
              chordCount = chordCount + 1
              if chordCount < chordMaxDisplay then
                if chordCount > chordStartDisplay then
                  scaleChordsForRootNote[chordCount] = chord
                else 
                  chordCount = chordCount - 1
                  chordStartDisplay = chordStartDisplay - 1
                end
              end
            end
          end
        end
      end
      if chordCount < 0 then
        -- this never happens yet, to do: rebuild the screen on overflow scroll correctly
        preferences.chordScroll.value = 0
        chordStartDisplay = 0
        clearChordBoxes()
        bRebuildScreen = true
      else
        bRebuildScreen = false
      end
    end
  end
  return scaleChordsForRootNote
end

function noteIsInScale(note)
  return scalePattern[getNotesIndex(note)]
end

function noteIsNotInScale(note)
  return not noteIsInScale(note)
end

function chordIsInScale(rootNote, chordIndex)

  local chord = chords[chordIndex]
  local chordPattern = chord['pattern']
  
  for i = 0, #chordPattern do
    local note = getNotesIndex(rootNote+i)
    if chordPattern:sub(i+1, i+1) == '1' and noteIsNotInScale(note) then
      return false
    end
  end
  
  return true
end

function noteIsInModalMixtureScale(note)

  local modalMixtureScalePattern = getScalePattern(preferences.scaleTonicNote.value, scales[preferences.modalMixtureScaleType.value])
  return modalMixtureScalePattern[getNotesIndex(note)]
end

function noteIsNotInModalMixtureScale(note)
  return not noteIsInModalMixtureScale(note)
end

function chordIsInModalMixtureScale(rootNote, chordIndex)

  local chord = chords[chordIndex]
  local chordPattern = chord['pattern']
  
  for i = 0, #chordPattern do
    local note = getNotesIndex(rootNote+i)
        
    if chordPattern:sub(i+1, i+1) == '1' and noteIsNotInModalMixtureScale(note) then
      return false
    end
  end
  
  return true
end

