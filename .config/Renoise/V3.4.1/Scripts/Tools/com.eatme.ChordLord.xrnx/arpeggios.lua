arpeggioNames = {
  { name = "Up Extra", pattern = "1" },
  { name = "Extra Up", pattern = "2" },
  { name = "Down Extra", pattern = "3" },
  { name = "Extra Down", pattern = "4" },
  { name = "Random Extra", pattern = "5" },
  { name = "Extra Random", pattern = "6" },
  { name = "Random", pattern = "7" }
}


