local dialog = nil
local vb = nil

class "RenoiseScriptingTool" (renoise.Document.DocumentNode)
function RenoiseScriptingTool:__init()    
  renoise.Document.DocumentNode.__init(self) 
  self:add_property("Name", "Untitled Tool")
  self:add_property("Version", "1.0")
end

local manifest = RenoiseScriptingTool()
local ok,err = manifest:load_from("manifest.xml")
local tool_name = manifest:property("Name").value
local tool_version = manifest:property("Version").value

prefs = renoise.Document.create {
  beats = 1,
  divisions = 3,
  note = 48,
}
renoise.tool().preferences = prefs

local function clear_lines(pattern_track, from, to)
  for i = from, to, 1 do
    pattern_track:line(i):clear()
  end
end

local function insert_notes(beats, divisions, note_value)
  local rs = renoise.song()
  if (rs.tracks[rs.selected_track_index].type ~= 1) then
    return
  end
  local edit_pos = rs.transport.edit_pos.line
  local track = rs.selected_track
  local pattern = rs.selected_pattern
  local pattern_track = rs.selected_pattern_track
  local number_of_lines = pattern.number_of_lines
  local lines = rs.transport.lpb * beats
  local lines_per_note = lines / divisions
  
  -- Make sure that we have enough note columns to contain everything
  local columns = math.min(12, math.max(1, math.ceil(divisions / lines)))
  if (track.visible_note_columns < columns) then
    track.visible_note_columns = columns
  end
  
  -- Show the delay column
  track.delay_column_visible = true
  
  -- Try to grab note from cursor position (if one exists)
  local note_instrument = rs.selected_instrument_index - 1
  local note_panning = 255
  local note_volume = 255
  local note_column_index = 1
  if (rs.selected_sub_column_type == renoise.Song.SUB_COLUMN_NOTE) then
    note_column_index = rs.selected_note_column_index
  end
  
  local note = pattern_track:line(edit_pos):note_column(note_column_index)
  if (not note.is_empty) and (note.note_value < 120) then
    note_value = note.note_value
    note_panning = note.panning_value
    note_volume = note.volume_value
    note_instrument = note.instrument_value
  end
  
  -- Clear old note data
  clear_lines(pattern_track, edit_pos, (edit_pos + lines) - 1)
  
  -- Generate new note data
  local column = 0
  local offset = nil
  local line = nil
  local delay = nil
  for i = 0, divisions - 1, 1 do
    offset = math.floor(i * lines_per_note)
    delay = math.floor(((i * lines_per_note) - offset) * 256)
    line = edit_pos + offset
    if (line > number_of_lines) then
      break
    end
    note = pattern_track:line(line):note_column(column + 1)
    note.instrument_value = note_instrument
    note.note_value = note_value
    note.volume_value = note_volume
    note.panning_value = note_panning
    note.delay_value = delay
    column = (column + 1) % columns
  end
  
end


local function show_dialog()

  if dialog and dialog.visible then
    dialog:show()
    return
  end
  
  local note_strings = {
    "C-", "C#", "D-", "D#", "E-", "F-", 
    "F#", "G-", "G#", "A-", "A#", "B-"
  }

  vb = renoise.ViewBuilder()
 
  local valuebox_beats = vb:valuebox {
    min = 1,
    max = 16,
    value = prefs.beats.value,
    notifier = function(value)
      prefs.beats.value = value
    end,
    tostring = function(value)
      return tostring(value)
    end,
    tonumber = function(str)
      return tonumber(str)
    end
  }
  
  local valuebox_divisions = vb:valuebox {
    min = 1,
    max = 64,
    value = prefs.divisions.value,
    notifier = function(value)
      prefs.divisions.value = value
    end,
    tostring = function(value)
      return tostring(value)
    end,
    tonumber = function(str)
      return tonumber(str)
    end
  }
  
  local valuebox_note = vb:valuebox {
    min = 0, 
    max = 119,
    value = prefs.note.value,
    notifier = function(value)
      prefs.note.value = value
    end,
    tostring = function(value) 
      local octave = math.floor(value / 12)
      local note = note_strings[ (value % 12) + 1 ]
      return ('%s%s'):format(note, octave)
    end,
    tonumber = function(str)
      local note_string = nil
      local octave = nil
      local note = 48
      note_string, octave = string.match(string.upper(str), '([ABCDEFG][\-#])([0-9])')
      if (note_string ~= nil) and (octave ~= nil) then
        for key, value in ipairs(note_strings) do
          if (note_string == value) then
            note = ((octave * 12) + key) - 1
            break
          end
        end 
      end
      return note
    end,
  }
    
  local content = vb:column {
    
    width = '100%',
    style = 'body',
    margin = 10,
    spacing = 10,  
            
    vb:row {    
      
      spacing = 10,    
            
      vb:column {
        vb:text {
          text = 'Beats:',
        },
        valuebox_beats,
      }, 
      
      vb:column {
        vb:text {
          text = 'Divisions:',
        },
        valuebox_divisions,
      },
     
      vb:column {
        vb:text {
          text = 'Note:',
        },
        valuebox_note,
      },
    
    },
       
    vb:row {
  
      vb:button {
        width = 200,
        height = 30,
        text = 'Go',
        notifier = function()
          insert_notes(valuebox_beats.value, valuebox_divisions.value, valuebox_note.value)
        end
      },
    },

  } 
  
  dialog = renoise.app():show_custom_dialog(tool_name .. ' v' .. tool_version, content)  
end

renoise.tool():add_menu_entry {
  name = "Pattern Editor:Track:Fractional Notes (Show GUI)",
  invoke = function()
    show_dialog()
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Track:Fractional Notes (Show GUI)",
  invoke = function()
    show_dialog()
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Track:Fractional Notes",
  invoke = function()
    insert_notes(prefs.beats.value, prefs.divisions.value, prefs.note.value)
  end
}
