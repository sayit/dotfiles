c.url.searchengines = {
    'DEFAULT':  'https://searx.be/search?q={}',
    '!a':       'https://www.amazon.com/s?k={}',
    '!d':       'https://duckduckgo.com/?ia=web&q={}',
    '!dd':      'https://thefreedictionary.com/{}',
    '!gh':      'https://github.com/search?o=desc&q={}&s=stars',
    '!gist':    'https://gist.github.com/search?q={}',
    '!r':       'https://www.reddit.com/search?q={}',
    '!t':       'https://www.thesaurus.com/browse/{}',
    '!tw':      'https://nitter.unixfox.eu/search?f=tweets&q={}&since=&until=&near=',
    '!w':       'https://en.wikipedia.org/wiki/{}',
    '!yt':      'https://www.youtube.com/results?search_query={}'
}

c.url.start_pages = 'https://searx.be/'

c.downloads.location.directory = "~/main/"
config.load_autoconfig()
