// ==UserScript==
// @name         Quizlet Show Hidden Flashcards
// @namespace    QuizletHack
// @version      15
// @description  Show the hidden flashcards in Quizlet without logging in.
// @author       hacker09
// @match        *://quizlet.com/*
// @icon         https://assets.quizlet.com/a/j/dist/i/favicon.6e263725c926227.ico
// @run-at       document-end
// @grant        none
// ==/UserScript==

(async function() {
  'use strict';
  var TimesExecuted = 0; //Creates a new variable
  var Counter = 0; //Creates a new counter variable
  var DisableReading = true; //If true the Reading function will be disabled
  var RemoveNeedlessThings = true; //If true the Needless Things will be removed

  async function hack() //Creates a new hack function
  { //Starts the new hack function
    //Show all hidden elements and disable the reading function
    document.querySelectorAll("div.SetPageTerm").forEach(function(FlashCards) { //For each FlashCard
      FlashCards.className = "SetPageTerm has-definitionText is-showing"; //Change the FlashCard element class name so that the card is shown
      if (DisableReading === true) //If the DisableReading variable is true
      { //Starts the if condition
        FlashCards.innerHTML = FlashCards.innerHTML; //Remove the audio of the FlashCard element
      } //Finishes the if condition
    }); //Finishes the foreach loop
  } //Finishes the hack function
  await hack(); //Calls the hack function

  window.onscroll = async function() { //Creates a new function to run when the page is scrolled
    TimesExecuted += 1; //Sum the amount of times that the page is scrolled
    if (TimesExecuted === 40) { //On the first time that the page is scrolled
      await hack(); //Calls the hack function

      setTimeout(function() { //Starts the settimeout function
        if (document.querySelector("#Executed") === null) //If the element wasn't added in the page yet
        { //Starts the if condition
          document.querySelector("div.SetPageTerm").insertAdjacentHTML('beforebegin', '<h1 style="cursor: pointer; margin-block: revert;" id="Executed">(Showing ' + document.querySelectorAll("div.SetPageTerm.has-definitionText.is-showing").length + ' Terms) Re-Run</h1>'); //Show the number of shown terms
          document.querySelector("h1#Executed").onclick = async function() { //Starts the onclick function
            document.querySelector("h1#Executed").remove(); //Removes the old number
            await hack(); // Calls the hack function
            document.querySelector("div.SetPageTerm").insertAdjacentHTML('beforebegin', '<h1 style="cursor: pointer; margin-block: revert;" id="Executed">(Showing ' + document.querySelectorAll("div.SetPageTerm.has-definitionText.is-showing").length + ' Terms) Re-Run</h1>'); //Show the new number of shown terms
          }; //Finishes the onclick function
          window.scrollTo(0, 0); //Scroll to top
        } //Finishes the if condition
      }, 1000); //Finishes the settimeout function

      if (RemoveNeedlessThings === true) //If the RemoveNeedlessThings variable is true
      { //Starts the if condition
        document.head.insertAdjacentHTML('beforeend', '<style>.SetPageStickyHeader.hgmlhdt, .SetPageTermsStickyBanner.SetPageTermsStickyBanner--hasAdz, .SetPageWall.SetPageWall--normal, .TopNavigationWrapper, .SetPage-setDetailsInfoWrapper, footer, .SetPage-setIntro, .wugyavo, .SetPage-setLinksWrapper, .SetPage-setIntroWrapper, .SetPageEmbeddedAd-wrapper, .SetPageTerms-embeddedDesktopAdz, .SetPageStudyModesBanner-body {display: none !important;}</style>'); //Hide the top menu, the stick footer banner, the Sign Up Box and the white overlay above this box, the top navigation menu, the user name that created the quiz, the needlessly big footer, the big flashcards box, the 2 stick footer banner, the related questions below all the cards, title of the cards set, big flash cards box and study/play columns below the big flash cars box, ads in between card rows, more ads, wants to learn this set quickly? question box below all card rows
        //.ReactModalPortal
        if (DisableReading === true) //If the DisableReading variable is true
        { //Starts the if condition
          document.querySelectorAll(".SetPageTerm-actions").forEach(function(FlashCards) { //For each star and audio btn
            Counter += 1; //Increase the Counter variable
            FlashCards.innerHTML = Counter; //Add a crescent number
          }); //Finishes the foreach loop
        } //Finishes the if condition

        if (document.querySelector("#UniversalUpsellTarget > div") !== null) //If the "save time with an expert" message element exists
        { //Starts the if condition
          document.querySelector("#UniversalUpsellTarget > div").remove(); //Remove the "save time with an expert" message
        } //Finishes the if condition

      } //Finishes the if condition
    } //Finishes the if condition
  }; //Finishes the onscroll event listener
})();
