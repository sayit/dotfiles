// ==UserScript==
// @name          Hacker News Dark
// @namespace     http://userstyles.org
// @description	  This style attempts to preserve the default view while being easier on the eyes. Neither the font styles nor the size and position of any elements on the page are affected.
// @author        Poorchop
// @homepage      https://userstyles.org/styles/113994
// @include       http://news.ycombinator.com/*
// @include       https://news.ycombinator.com/*
// @include       http://*.news.ycombinator.com/*
// @include       https://*.news.ycombinator.com/*
// @run-at        document-start
// @version       0.20210512024743
// ==/UserScript==
(function() {var css = [
	"@namespace url(http://www.w3.org/1999/xhtml);",
	"body {",
	"    background-color: #262626 !important;",
	"}",
	"",
	"body > center > table, input, textarea {",
	"    background-color: #222 !important;",
	"}",
	"",
	"body > center > table > tbody > tr:first-child > td {",
	"    background-color: #ff6600 !important;",
	"}",
	"",
	"/* Bright text */",
	"td.title a:link, span.comment font, span.comment font a:link, u a:link, span.yclinks a:link, body:not([id]),",
	"td:nth-child(2):not(.subtext) > a:link, input, textarea, p > a, a > u, .c00, .c00 a:link,",
	"a[href=\"http://www.ycombinator.com/apply/\"], a[href=\"https://www.ycombinator.com/apply/\"] {",
	"    color: #ccc !important;",
	"}",
	"",
	".admin td {",
	"    color: #aaa !important;",
	"}",
	"",
	"/* search box and comment box */",
	"input, textarea {",
	"    border: 1px solid #828282 !important;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();

